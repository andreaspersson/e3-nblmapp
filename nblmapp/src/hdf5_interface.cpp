/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by DMCS for the European Spallation Source ERIC
 * Author: Grzegorz Jablonski
 * 		   DMCS, Lodz, Poland
 * 
 * Modified by CEA-ESS for the European Spallation Source ERIC
 * Author: Yannick Mariette
 *         CEA, Saclay, France
 */
#ifdef __x86_64__

#include "hdf5_interface.h"
#include <memory>

#define GET_SIZE_FROM_FILE 0
#define TEST_RECORDS 10000000
#define DO_NOT_RESIZE 0

const size_t BUF_THRESHOLD = 1024;
//#define DATASET_MAX_SIZE  (long unsigned int)(1024*BUF_THRESHOLD)
#define MAX_STR_SIZE 50

#ifndef USE_NAPI
const int RANK = 1;
std::mutex fileAccess_mutex;

template <class T>
void hdf5_for_one_ifc::append_HDF5_data(std::vector<T>& b, const char *datasetPathAndName, unsigned long long & offsetWhereToWrite, hid_t& type)
{
  hid_t   file_id, filespace, dataset_id, append_dataspace_id;  /* identifiers */
  hsize_t dims1[RANK] = { b.size() };
  hsize_t offset[RANK] = { offsetWhereToWrite };
  hsize_t newdims[RANK] = {offsetWhereToWrite + b.size()};

    // Write in file when no task is being writing
    std::lock_guard<std::mutex> lock(fileAccess_mutex);

    file_id = H5Fopen(HDF_FILE.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);

    // Open a dataset in group "entry" within the file.
    dataset_id = H5Dopen(file_id, datasetPathAndName, H5P_DEFAULT);

    // Extend the dataset.
    H5Dset_extent(dataset_id, newdims);

    // Select a hyperslab.
    filespace = H5Dget_space(dataset_id);
    H5Sselect_hyperslab(filespace, H5S_SELECT_SET, offset, NULL,
				 dims1, NULL);

    // Define memory space
    append_dataspace_id = H5Screate_simple(RANK, dims1, NULL);

    // Write the data to the hyperslab
    H5Dwrite(dataset_id, type, append_dataspace_id, filespace, H5P_DEFAULT, &b[0]);

    // Close/release resources.
    H5Dclose(dataset_id);
    H5Sclose(filespace);
    H5Fclose(file_id);
    H5Sclose(append_dataspace_id);

    offsetWhereToWrite += b.size();
    b.clear();
}


void hdf5_for_one_ifc::writeTimestampAndSampleToHdf (const hdf5_timestampAndSample * data)
{
  //char path_dataset_rawdata[MAX_STR_SIZE];
  const char* const  path_dataset_rawdata= "/entry/data/rawdata0";
  
  timestampAndSample_buffer.push_back(data[0]);
  timestampAndSample_buffer.push_back(data[1]);

  //sprintf(path_dataset_rawdata, "/entry/data/rawdata%ld", index_chunk_dataset);

  if (timestampAndSample_buffer.size() > BUF_THRESHOLD)
  {
    // // Set if(0) if we don't want Dataset chunk
    // if(0)
    // //if(timestampAndSample_dataset_size > DATASET_MAX_SIZE)
    // {
    //   hid_t       file_id, dataset1_id, cparms;  /* identifiers */
    //   hsize_t     maxdims[RANK] = {H5S_UNLIMITED};
    //   hsize_t     dims[RANK]= {0};
    //   hsize_t     chunk_dims[RANK] = {BUF_THRESHOLD};

    //   // Create the data space with unlimited dimensions.
    //   dataspace_id = H5Screate_simple(RANK, dims, maxdims);

    //   index_chunk_dataset++;
    //   sprintf(path_dataset_rawdata, "/entry/data/rawdata%ld", index_chunk_dataset);

    //   file_id = H5Fopen(HDF_FILE.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
      
    //   // Modify dataset creation properties, i.e. enable chunking.
    //   cparms = H5Pcreate(H5P_DATASET_CREATE);
    //   H5Pset_chunk( cparms, RANK, chunk_dims);

    //   // Create datasets in group "entry/data" within the file using cparms creation properties.
    //   dataset1_id = H5Dcreate2(file_id, path_dataset_rawdata, hdf5_timestampAndSample_id, dataspace_id, H5P_DEFAULT, cparms, H5P_DEFAULT);

    //   H5Pclose(cparms);
    //   H5Dclose(dataset1_id);

    //   H5Fclose(file_id);
    //   H5Sclose(dataspace_id);
    //   timestampAndSample_dataset_size = 0;
    //   lastTimestampAndSampleIndex = 0;
    // }
    
    timestampAndSample_dataset_size += timestampAndSample_buffer.size();
    append_HDF5_data<hdf5_timestampAndSample>(timestampAndSample_buffer, path_dataset_rawdata, lastTimestampAndSampleIndex, hdf5_timestampAndSample_id);
    //printf("debug %s: timestampAndSample_dataset_size(%d) = %llu\n", __PRETTY_FUNCTION__, m_AM_Channel, timestampAndSample_dataset_size);
  }
}

void hdf5_for_one_ifc::writeNeutronCountToHdf (const hdf5_neutronCount & data)
{
  neutronCount_buffer.push_back(data);
  if (neutronCount_buffer.size() > BUF_THRESHOLD)
  {
    neutronCount_dataset_size += neutronCount_buffer.size();
    append_HDF5_data<hdf5_neutronCount>(neutronCount_buffer, "/entry/data/neutronCount", lastNeutronCountIndex, hdf5_neutronCount_id);
  }
}


void hdf5_for_one_ifc::writeEventInfoToHdf (const hdf5_eventInfo & data)
{
  eventInfo_buffer.push_back(data);
  if (eventInfo_buffer.size() > BUF_THRESHOLD)
  {
    eventInfo_dataset_size += eventInfo_buffer.size();
    append_HDF5_data<hdf5_eventInfo>(eventInfo_buffer, "/entry/data/eventInfo", lastEventInfoIndex, hdf5_eventInfo_id);
  }
}

void hdf5_for_one_ifc::writeTriggerEventInfoToHdf (const hdf5_triggerEventInfo & data)
{
  triggerEventInfo_buffer.push_back(data);
  if (triggerEventInfo_buffer.size() > BUF_THRESHOLD)
  {
     triggerEventInfo_dataset_size += triggerEventInfo_buffer.size();
     append_HDF5_data<hdf5_triggerEventInfo>(triggerEventInfo_buffer, "/entry/data/triggerEventInfo", lastTriggerEventInfoIndex, hdf5_triggerEventInfo_id);
  }
}

void hdf5_for_one_ifc::flush_one_HDF5(void)
{
  //char path_dataset_rawdata[MAX_STR_SIZE];
  const char* const  path_dataset_rawdata= "/entry/data/rawdata0";

  std::cout <<  "flush HDF5 " << HDF_FILE << std::endl;

  //sprintf(path_dataset_rawdata, "/entry/data/rawdata%ld", index_chunk_dataset);
  printf("debug %s: m_AM_Channel = %d\n", __PRETTY_FUNCTION__, m_AM_Channel);

  if (timestampAndSample_buffer.size())
  {
    timestampAndSample_dataset_size += timestampAndSample_buffer.size();
    append_HDF5_data<hdf5_timestampAndSample>(timestampAndSample_buffer, path_dataset_rawdata, lastTimestampAndSampleIndex, hdf5_timestampAndSample_id);
    printf("debug %s: timestampAndSample_dataset_size(%d) = %llu\n", __PRETTY_FUNCTION__, m_AM_Channel, timestampAndSample_dataset_size);
  }

  if (neutronCount_buffer.size())
  {
    neutronCount_dataset_size += neutronCount_buffer.size();
    append_HDF5_data<hdf5_neutronCount>(neutronCount_buffer, "/entry/data/neutronCount", lastNeutronCountIndex, hdf5_neutronCount_id);
    printf("debug %s: neutronCount_dataset_size(%d) = %llu\n", __PRETTY_FUNCTION__, m_AM_Channel, neutronCount_dataset_size);
  }

  if (eventInfo_buffer.size())
  {
    eventInfo_dataset_size += eventInfo_buffer.size();
    append_HDF5_data<hdf5_eventInfo>(eventInfo_buffer, "/entry/data/eventInfo", lastEventInfoIndex, hdf5_eventInfo_id); 
    printf("debug %s: eventInfo_dataset_size(%d) = %llu\n", __PRETTY_FUNCTION__, m_AM_Channel, eventInfo_dataset_size);
  }

  if (triggerEventInfo_buffer.size())
  {
    triggerEventInfo_dataset_size += triggerEventInfo_buffer.size();
    append_HDF5_data<hdf5_triggerEventInfo>(triggerEventInfo_buffer, "/entry/data/triggerEventInfo", lastTriggerEventInfoIndex, hdf5_triggerEventInfo_id); 
    printf("debug %s: triggerEventInfo_dataset_size(%d) = %llu\n", __PRETTY_FUNCTION__, m_AM_Channel, triggerEventInfo_dataset_size);
  }
};

void hdf5_for_one_ifc::init_one_HDF5(void) {

   hid_t       file_id, cparms, group_id;  /* identifiers */
   hsize_t     chunk_dims[RANK] = {BUF_THRESHOLD};
   hid_t       dataset1_id, dataset2_id, dataset3_id, dataset4_id;
   hsize_t     maxdims[RANK] = {H5S_UNLIMITED};
   hsize_t     dims[RANK]= {0};
 #define FILE_ATTR_NB     5
 #define NXENTRY_ATTR_NB  4
 #define NXDATA_ATTR_NB   4
 #define DATASET_ATTR_NB  9
   hid_t       attr_dataspace, file_attr_type[FILE_ATTR_NB], file_attr[FILE_ATTR_NB];
   hid_t       NXentry_attr_type[NXENTRY_ATTR_NB], NXentry_attr[NXENTRY_ATTR_NB];
   hid_t       NXdata_attr_type[NXDATA_ATTR_NB], NXdata_attr[NXDATA_ATTR_NB];
   hid_t       dataset_attr_type[DATASET_ATTR_NB], dataset_attr[DATASET_ATTR_NB];
   
   const char* file_attr_name[] =          { "Serial number of the nBLM detector",
                                             "Production date",
                                             "Mesure date",
                                             "Responsible",
                                             "Facility/institute name"};
   
   const char* file_attr_val[] =           { "0000000001",
                                             "2020/01/01",
                                             "2020/01/01",
                                             "Irena Dolenc Kittelmann",
                                             "ESS BI"};

   const char* NXentry_attr_name[] =       { "NX_Class",
                                             "Sweep conditions",
                                             "Data type",
                                             "Device channel number"};
   
   char NXentry_attr_val[][MAX_STR_SIZE] = { "NXentry",
                                             "Not applicable",
                                             "Measurement data",
                                             "AIx"};

   const char* NXdata_attr_name[] =        { "NX_Class",
                                             "Sweep conditions",
                                             "Data type",
                                             "Device channel number"};
   
   char NXdata_attr_val[][MAX_STR_SIZE] =  { "NXdata",
                                             "Not applicable",
                                             "Measurement data",
                                             "AIx"};

   const char* dataset_attr_name[] =       { "Laboratory device 1",
                                             "Laboratory device 2",
                                             "Laboratory device 3",
                                             "Physical units",
                                             "Sample rate Hz",
                                             "Trigger samples",
                                             "PV name",
                                             "ESS system name",
                                             "Measurement timestamp"};
   
   const char* dataset_attr_val[] =        { "ESS nBLM test stand",
                                             "IOxOS ADC_3111",
                                             "Generator waveform",
                                             "ADU 16bits -0.5[V] to +0.5[V]",
                                             "250 MHz",
                                             "See dimension size (General object info)",
                                             "Not applicable",
                                             "Not applicable",
                                             "See first data in triggerEventInfo dataset"};


  // Write in file when no task is being writing
  std::lock_guard<std::mutex> lock(fileAccess_mutex);

  HDF_FILE = m_deviceUser.hdf5Name + std::string("_output") + std::to_string(m_AM_Channel) + std::string(".h5");
  std::cout <<  "init HDF5 " << HDF_FILE << std::endl;

  index_chunk_dataset = 0;
  timestampAndSample_dataset_size = 0;

  lastTimestampAndSampleIndex = 0;
  lastNeutronCountIndex= 0;
  lastEventInfoIndex = 0;
  lastTriggerEventInfoIndex = 0;

  // Create the data space for attributes
  attr_dataspace = H5Screate(H5S_SCALAR);
  // Create the data space with unlimited dimensions.
  dataspace_id = H5Screate_simple(RANK, dims, maxdims);

  //////////////// Create a new file using default properties. //////////////////
  file_id = H5Fcreate(HDF_FILE.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);


  ///////////////////// Setting attributes at File level ////////////////////////
  for(uint8_t i=0; i<FILE_ATTR_NB; i++)
  {
    file_attr_type[i] = H5Tcreate(H5T_STRING, strlen(file_attr_val[i]));
    H5Tset_cset(file_attr_type[i], H5T_CSET_ASCII);
    file_attr[i] = H5Acreate2(file_id, file_attr_name[i], file_attr_type[i], attr_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(file_attr[i], file_attr_type[i], file_attr_val[i]);
    H5Aclose(file_attr[i]);
    H5Tclose(file_attr_type[i]);
  }


  ////////////// Create a group named "/entry" in the root group. ///////////////////
  group_id = H5Gcreate2(file_id, "/entry", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);


  ///////////////////// Setting attributes at NXentry level ////////////////////////
  // // Update the ADC number : NOT THE ADC CHANNEL HERE, IT IS IN FACT DATA PROCESSING CHANNEL....
  NXentry_attr_val[3][2] = (char)(m_AM_Channel+0x30);
  for(uint8_t i=0; i<NXENTRY_ATTR_NB; i++)
  {
    NXentry_attr_type[i] = H5Tcreate(H5T_STRING, MAX_STR_SIZE);
    H5Tset_cset(NXentry_attr_type[i], H5T_CSET_ASCII);
    NXentry_attr[i] = H5Acreate2(group_id, NXentry_attr_name[i], NXentry_attr_type[i], attr_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(NXentry_attr[i], NXentry_attr_type[i], NXentry_attr_val[i]);
    H5Aclose(NXentry_attr[i]);
    H5Tclose(NXentry_attr_type[i]);
  }

  // Close the group. 
  H5Gclose(group_id);

  ///////////// Create a group named "/data" in the group "/entry". /////////////////
  group_id = H5Gcreate2(file_id, "/entry/data", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  ///////////////////// Setting attributes at NXdata level ////////////////////////
  // // Update the ADC number
  NXdata_attr_val[3][2] = (char)(m_AM_Channel+0x30);
  for(uint8_t i=0; i<NXDATA_ATTR_NB; i++)
  {
    NXdata_attr_type[i] = H5Tcreate(H5T_STRING, MAX_STR_SIZE);
    H5Tset_cset(NXdata_attr_type[i], H5T_CSET_ASCII);
    NXdata_attr[i] = H5Acreate2(group_id, NXdata_attr_name[i], NXdata_attr_type[i], attr_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(NXdata_attr[i], NXdata_attr_type[i], NXdata_attr_val[i]);
    H5Aclose(NXdata_attr[i]);
    H5Tclose(NXdata_attr_type[i]);
  }

  // Close the group. 
  H5Gclose(group_id);


  // Modify dataset creation properties, i.e. enable chunking.
  cparms = H5Pcreate(H5P_DATASET_CREATE);
  H5Pset_chunk( cparms, RANK, chunk_dims);


  // Creating a dataset with a compound datatype.
  hdf5_timestampAndSample_id = H5Tcreate (H5T_COMPOUND, sizeof(hdf5_timestampAndSample));
  H5Tinsert(hdf5_timestampAndSample_id, "evt_timestamp", HOFFSET(hdf5_timestampAndSample, evt_timestamp), H5T_NATIVE_UINT64);
  H5Tinsert(hdf5_timestampAndSample_id, "timestamp", HOFFSET(hdf5_timestampAndSample, timestamp), H5T_NATIVE_UINT32);
  H5Tinsert(hdf5_timestampAndSample_id, "sample", HOFFSET(hdf5_timestampAndSample, sample), H5T_NATIVE_UINT16);    

  // Creating a 2nd dataset with a compound datatype.
  hdf5_neutronCount_id = H5Tcreate (H5T_COMPOUND, sizeof(hdf5_neutronCount));
  H5Tinsert(hdf5_neutronCount_id, "evt_timestamp", HOFFSET(hdf5_neutronCount, evt_timestamp), H5T_NATIVE_UINT64);
  H5Tinsert(hdf5_neutronCount_id, "MTWindx", HOFFSET(hdf5_neutronCount, MTWindx), H5T_NATIVE_UINT32);
  H5Tinsert(hdf5_neutronCount_id, "q_N", HOFFSET(hdf5_neutronCount, q_N), H5T_NATIVE_INT32); 
  H5Tinsert(hdf5_neutronCount_id, "background_count", HOFFSET(hdf5_neutronCount, background_count), H5T_NATIVE_INT32); 
  H5Tinsert(hdf5_neutronCount_id, "n", HOFFSET(hdf5_neutronCount, n), H5T_NATIVE_UINT8); 
  H5Tinsert(hdf5_neutronCount_id, "negative_saturations", HOFFSET(hdf5_neutronCount, negative_saturations), H5T_NATIVE_UINT8); 
  H5Tinsert(hdf5_neutronCount_id, "positive_saturations", HOFFSET(hdf5_neutronCount, positive_saturations), H5T_NATIVE_UINT8); 
  H5Tinsert(hdf5_neutronCount_id, "CB_channel", HOFFSET(hdf5_neutronCount, channel), H5T_NATIVE_UINT8); 

  // Creating a 3rd dataset with a compound datatype.
  hdf5_eventInfo_id = H5Tcreate (H5T_COMPOUND, sizeof(hdf5_eventInfo));
  H5Tinsert(hdf5_eventInfo_id, "evt_timestamp", HOFFSET(hdf5_eventInfo, evt_timestamp), H5T_NATIVE_UINT64);
  H5Tinsert(hdf5_eventInfo_id, "MTWindx", HOFFSET(hdf5_eventInfo, MTWindx), H5T_NATIVE_UINT32); 
  H5Tinsert(hdf5_eventInfo_id, "Q_TOT", HOFFSET(hdf5_eventInfo, Q_TOT), H5T_NATIVE_INT32);
  H5Tinsert(hdf5_eventInfo_id, "peakValue", HOFFSET(hdf5_eventInfo, peakValue), H5T_NATIVE_INT32);
  H5Tinsert(hdf5_eventInfo_id, "TOT", HOFFSET(hdf5_eventInfo, TOT), H5T_NATIVE_UINT16);
  H5Tinsert(hdf5_eventInfo_id, "TOTstartTime", HOFFSET(hdf5_eventInfo, TOTstartTime), H5T_NATIVE_UINT16);
  H5Tinsert(hdf5_eventInfo_id, "peakTime", HOFFSET(hdf5_eventInfo, peakTime), H5T_NATIVE_UINT16);
  H5Tinsert(hdf5_eventInfo_id, "serialNumber", HOFFSET(hdf5_eventInfo, serialNumber), H5T_NATIVE_UINT16);
  H5Tinsert(hdf5_eventInfo_id, "flags", HOFFSET(hdf5_eventInfo, flags), H5T_NATIVE_UINT8);
  H5Tinsert(hdf5_eventInfo_id, "CB_channel", HOFFSET(hdf5_eventInfo, channel), H5T_NATIVE_UINT8);

  // Creating a 4th dataset with a compound datatype.
  hdf5_triggerEventInfo_id = H5Tcreate (H5T_COMPOUND, sizeof(hdf5_triggerEventInfo));
  H5Tinsert(hdf5_triggerEventInfo_id, "int_timestamp", HOFFSET(hdf5_triggerEventInfo, int_timestamp), H5T_NATIVE_UINT64); 
  H5Tinsert(hdf5_triggerEventInfo_id, "evt_timestamp", HOFFSET(hdf5_triggerEventInfo, evt_timestamp), H5T_NATIVE_UINT64); 
  H5Tinsert(hdf5_triggerEventInfo_id, "CB_channel", HOFFSET(hdf5_triggerEventInfo, channel), H5T_NATIVE_UINT8); 

  
  ///////////////////// Create dataset1 in group "entry/data". ////////////////////
  dataset1_id = H5Dcreate2(file_id, "/entry/data/rawdata0", hdf5_timestampAndSample_id, dataspace_id, H5P_DEFAULT, cparms, H5P_DEFAULT);

  ///////////////////// Setting attributes at dataset1 level ////////////////////////
  for(uint8_t i=0; i<DATASET_ATTR_NB; i++)
  {
    dataset_attr_type[i] = H5Tcreate(H5T_STRING, strlen(dataset_attr_val[i]));
    H5Tset_cset(dataset_attr_type[i], H5T_CSET_ASCII);
    dataset_attr[i] = H5Acreate2(dataset1_id, dataset_attr_name[i], dataset_attr_type[i], attr_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(dataset_attr[i], dataset_attr_type[i], dataset_attr_val[i]);
    H5Aclose(dataset_attr[i]);
    H5Tclose(dataset_attr_type[i]);
  }

  H5Dclose(dataset1_id);

  ///////////////////// Create dataset2 in group "entry/data". ////////////////////
  dataset2_id = H5Dcreate2(file_id, "/entry/data/neutronCount", hdf5_neutronCount_id, dataspace_id, H5P_DEFAULT, cparms, H5P_DEFAULT);

  ///////////////////// Setting attributes at dataset2 level ////////////////////////
  for(uint8_t i=0; i<DATASET_ATTR_NB; i++)
  {
    dataset_attr_type[i] = H5Tcreate(H5T_STRING, strlen(dataset_attr_val[i]));
    H5Tset_cset(dataset_attr_type[i], H5T_CSET_ASCII);
    dataset_attr[i] = H5Acreate2(dataset2_id, dataset_attr_name[i], dataset_attr_type[i], attr_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(dataset_attr[i], dataset_attr_type[i], dataset_attr_val[i]);
    H5Aclose(dataset_attr[i]);
    H5Tclose(dataset_attr_type[i]);
  }

  H5Dclose(dataset2_id);

  ///////////////////// Create dataset3 in group "entry/data". ////////////////////
  dataset3_id = H5Dcreate2(file_id, "/entry/data/eventInfo", hdf5_eventInfo_id, dataspace_id, H5P_DEFAULT, cparms, H5P_DEFAULT);

  ///////////////////// Setting attributes at dataset3 level ////////////////////////
  for(uint8_t i=0; i<DATASET_ATTR_NB; i++)
  {
    dataset_attr_type[i] = H5Tcreate(H5T_STRING, strlen(dataset_attr_val[i]));
    H5Tset_cset(dataset_attr_type[i], H5T_CSET_ASCII);
    dataset_attr[i] = H5Acreate2(dataset3_id, dataset_attr_name[i], dataset_attr_type[i], attr_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(dataset_attr[i], dataset_attr_type[i], dataset_attr_val[i]);
    H5Aclose(dataset_attr[i]);
    H5Tclose(dataset_attr_type[i]);
  }

  H5Dclose(dataset3_id);

  ///////////////////// Create dataset4 in group "entry/data". ////////////////////
  dataset4_id = H5Dcreate2(file_id, "/entry/data/triggerEventInfo", hdf5_triggerEventInfo_id, dataspace_id, H5P_DEFAULT, cparms, H5P_DEFAULT);

  ///////////////////// Setting attributes at dataset4 level ////////////////////////
  for(uint8_t i=0; i<DATASET_ATTR_NB; i++)
  {
    dataset_attr_type[i] = H5Tcreate(H5T_STRING, strlen(dataset_attr_val[i]));
    H5Tset_cset(dataset_attr_type[i], H5T_CSET_ASCII);
    dataset_attr[i] = H5Acreate2(dataset4_id, dataset_attr_name[i], dataset_attr_type[i], attr_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(dataset_attr[i], dataset_attr_type[i], dataset_attr_val[i]);
    H5Aclose(dataset_attr[i]);
    H5Tclose(dataset_attr_type[i]);
  }

  H5Dclose(dataset4_id);

  // Close/release resources.
  H5Pclose(cparms);
  H5Fclose(file_id);
  H5Sclose(dataspace_id);
  H5Sclose(attr_dataspace);
}


// void test(void) // from unicode-1.c at https://portal.hdfgroup.org/display/HDF5/Other+Examples
// {
//   char* vname = "\xce\xbc-vlen";         /* mu-vlen */
//   char* fname = "\xce\xbc-fix";          /* mu-fix */
//   char* value = "r\xc3\xa9sum\xc3\xa9";  /* resume (with accents) */

//   hid_t fapl, acpl, vlenstrtype, fixstrtype, fsp, file, attr;

//   fapl = H5Pcreate(H5P_FILE_ACCESS);
//   H5Pset_libver_bounds(fapl, H5F_LIBVER_LATEST, H5F_LIBVER_LATEST);

//   acpl = H5Pcreate(H5P_ATTRIBUTE_CREATE);
//   H5Pset_char_encoding(acpl, H5T_CSET_UTF8);

//   vlenstrtype = H5Tcreate(H5T_STRING, H5T_VARIABLE);
//   H5Tset_cset(vlenstrtype, H5T_CSET_UTF8);

//   fixstrtype = H5Tcreate(H5T_STRING, 10);
//   H5Tset_cset(fixstrtype, H5T_CSET_UTF8);

//   fsp = H5Screate(H5S_SCALAR);

//   file = H5Fcreate("unicode.h5", H5F_ACC_TRUNC, H5P_DEFAULT, fapl);

//   // vlen-string attribute
//   attr = H5Acreate2(file, vname, vlenstrtype, fsp, acpl, H5P_DEFAULT);
//   H5Awrite(attr, vlenstrtype, &value);
//   H5Aclose(attr);

//   // fixed-length string attribute
//   attr = H5Acreate2(file, fname, fixstrtype, fsp, acpl, H5P_DEFAULT);
//   H5Awrite(attr, fixstrtype, value);
//   H5Aclose(attr);

//   // clean house

//   H5Sclose(fsp);
//   H5Fclose(file);
//   H5Tclose(fixstrtype);
//   H5Tclose(vlenstrtype);
//   H5Pclose(acpl);
//   H5Pclose(fapl);
// }


// void init_HDF5(void) {
//   for(uint8_t m_AM_Channel=0; m_AM_Channel < AM_CHANNEL_NB_MAX; m_AM_Channel++)
//     init_one_HDF5(m_AM_Channel);
// }



//#define MAIN_C
#ifdef MAIN_C

std::vector<hdf5_timestampAndSample> sample_compound_buffer;

int main() {

long long unsigned int offset1=0, offset2=0, offset3=0;

   for(int adcNb=0; adcNb<AM_CHANNEL_NB_MAX; adcNb++)
   {
    init_one_HDF5(adcNb);
    
    // Initialize the first dataset. 
    hdf5_timestampAndSample data;
    for (unsigned long int m = 0; m < 2000000; m++)
    {
      data.sample = 2*m*adcNb;
      data.timestamp = m;
      writeTimestampAndSampleToHdf (data, adcNb);
    }
   }

  return 0;
}
#endif

#else // else of #ifndef USE_NAPI

//////////////////////////////////////
// using Nexus API (NAPI)           //
//////////////////////////////////////
template<class T> void append_HDF5_data(std::vector<T>& b, const char *datasetName, int *slab_start, uint8_t m_AM_Channel)
{
  NXhandle file_id;
  char groupName[20];
  const int RANK = 1;
  int slab_size[RANK];

  sprintf(groupName, "entryChIFC1410:AI%d", m_AM_Channel);
  for(int i=0; i< RANK; i++)
    slab_size[i] = b.size();

  // Open output file and output global attributes
  NXopen ("AANXfile.nxs", NXACC_RDWR, &file_id);
  // Open top-level NXentry group
    NXopengroup (file_id, groupName, "NXentry");
  // Open NXdata group within NXentry group
      NXopengroup (file_id, "data", "NXdata");
  // Output data
        NXopendata (file_id, datasetName);
        NXputslab (file_id, &b[0], slab_start, slab_size);
        NXclosedata (file_id);
        for(int i=0; i< RANK; i++)
          slab_start[i] += slab_size[i];
  // Close NXentry and NXdata groups and close file
      NXclosegroup (file_id);
    NXclosegroup (file_id);
  NXclose (&file_id);

  b.clear();
}


int main() {
int counts[50][1000], n_t=1000, n_p=50, dims[2], i;
float t[1000]={1}, phi[50], ajout[10];
NXhandle file_id;
const int RANK = 1;
int ext_dim[1] = {NX_UNLIMITED}, slab_start_rawData[1], slab_start_timestamp[1];
/*
*Read in data using local routines to populate phi and counts
*
*for example you may create a getdata() function and call
*
*getdata (n_t, t, n_p, phi, counts);
*/

  slab_start_rawData[0] = 0;
  slab_start_timestamp[0] = 0;


/*Open output file and output global attributes*/
NXopen ("AANXfile.nxs", NXACC_CREATE5, &file_id);
  NXputattr (file_id, "user_name", "Yannick Mariette", 16, NX_CHAR);
/*Open top-level NXentry group*/
  NXmakegroup (file_id, "entryChIFC1410:AI0", "NXentry");
  NXopengroup (file_id, "entryChIFC1410:AI0", "NXentry");
/*Open NXdata group within NXentry group*/
    NXmakegroup (file_id, "data", "NXdata");
    NXopengroup (file_id, "data", "NXdata");
/*Output time channels*/
      NXmakedata (file_id, "data0", NX_FLOAT32, RANK, ext_dim);
      NXopendata (file_id, "data0");
        NXputattr (file_id, "units", "millivolts", strlen("millivolts"), NX_CHAR);
      NXclosedata (file_id);

      NXmakedata (file_id, "timestamp0", NX_FLOAT32, RANK, ext_dim);
      NXopendata (file_id, "timestamp0");
        NXputattr (file_id, "units", "nanoseconds", strlen("nanoseconds"), NX_CHAR);
      NXclosedata (file_id);

// /*Output data*/
//       dims[0] = n_t;
//       dims[1] = n_p;
//       NXmakedata (file_id, "counts", NX_INT32, 2, dims);
//       NXopendata (file_id, "counts");
//         NXputdata (file_id, counts);
//         i = 1;
//         NXputattr (file_id, "signal", &i, 1, NX_INT32);
//         NXputattr (file_id, "axes",  "polar_angle:time_of_flight", 26, NX_CHAR);
//       NXclosedata (file_id);
/*Close NXentry and NXdata groups and close file*/
    NXclosegroup (file_id);
  NXclosegroup (file_id);
NXclose (&file_id);

   std::vector<float> buffer;
   for (uint16_t i = 0; i < BUF_THRESHOLD; i++)
   {
    buffer.push_back(i);
   }


  //  // Initialize the first dataset. 
  //  std::vector<hdf5_timestampAndSample> sample_compound_buffer;
  //  hdf5_timestampAndSample k;
  //  for (uint16_t i = 0; i < BUF_THRESHOLD; i++)
  //  {
  //   k.sample = i;
  //   k.timestamp = 500 + i;
  //   sample_compound_buffer.push_back(k);
  //  }
append_HDF5_data<float>(buffer, "timestamp0", slab_start_timestamp, 0);

   buffer.clear();
   for (uint16_t i = 0; i < BUF_THRESHOLD; i++)
   {
    buffer.push_back(i*i);
   }
append_HDF5_data<float>(buffer, "data0", slab_start_rawData, 0);


return 0;
}
#endif // end of #ifndef USE_NAPI

#endif //of #ifdef __x86_64__

