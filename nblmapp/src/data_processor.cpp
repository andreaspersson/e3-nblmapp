/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by DMCS for the European Spallation Source ERIC
 * Author: Grzegorz Jablonski
 * 		   DMCS, Lodz, Poland
 * 
 * Modified by CEA-ESS for the European Spallation Source ERIC
 * Author: Yannick Mariette
 *         CEA, Saclay, France
 */
#include "data_processor.h"
#ifdef ENABLE_DATA_CHECK
#include "algo_prepare_reference.h"
#endif
#include "IFC14AIChannelGroup.h"

void
DataProcessor::process_event (struct u256 *d, timestamp_triple range[2])
{


  struct eventInfoForArchiving ei;

  ei.MTWindx = u256_get_top_bits_and_cut (d, 32);	// 32 bits
  ei.Q_TOT = u256_get_top_bits_and_cut (d, 26);	// 26 bits 
  ei.TOT = u256_get_top_bits_and_cut (d, 9);	// 9 bits
  ei.TOTlimitReached = u256_get_top_bits_and_cut (d, 1);	// 1 bit
  ei.TOTstartTime = u256_get_top_bits_and_cut (d, 9);	// 9 bits 
  ei.TOTvalid = u256_get_top_bits_and_cut (d, 1);	// 1 bit
  ei.isPart2 = u256_get_top_bits_and_cut (d, 1);	// 1 bit
  ei.peakTime = u256_get_top_bits_and_cut (d, 9);	// 9 bits
  ei.peakValid = u256_get_top_bits_and_cut (d, 1);	// 1 bit
  ei.peakValue = u256_get_top_bits_and_cut (d, 17);	// 17 bits
  ei.pileUp = u256_get_top_bits_and_cut (d, 1);	// 1 bit
  ei.serialNumber = u256_get_top_bits_and_cut (d, 13);	// 13 bits

  ei.eventValid = true;

  if ((prev_MTWindx > UINT32_MAX / 2) && (ei.MTWindx < UINT32_MAX / 2))
    wraparounds++;

  prev_MTWindx = ei.MTWindx;

  int_timestamp st;
  st.MTW = ei.MTWindx;
  st.sample = ei.TOTstartTime;
  uint64_t evt_ts = get_evt_ts(st);


  AIChannels.at(ch)->readEventDetection(ei);

#ifdef USE_HDF5
if(enableHDF5)
{
 hdf5_eventInfo di;
 
 di.evt_timestamp = evt_ts;
 di.MTWindx = ei.MTWindx;
 di.Q_TOT = ei.Q_TOT;
 di.peakValue = ei.peakValue;
 di.TOT = ei.TOT;
 di.TOTstartTime = ei.TOTstartTime;
 di.peakTime = ei.peakTime;
 di.serialNumber = ei.serialNumber;
 di.flags = 0;
 if(ei.TOTvalid)
   di.flags |= 1;
 if(ei.peakValid)
   di.flags |= 2;
 if(ei.pileUp)
   di.flags |= 4;
 if(ei.isPart2)
   di.flags |= 8;
 di.channel = ch;
  
  AIChannels.at(ch)->hdf5Obj->writeEventInfoToHdf (di);
}
#endif
#ifdef USE_TXTFILES
  fprintf (outfile,
           "%" PRIu64 " %d %d %d %d %d %d %d %d %d %d %" PRIu32 " %d\n", evt_ts, (int) ei.TOTvalid, (int) ei.peakValid, (int) ei.pileUp,
           (int) ei.TOTlimitReached, (int) ei.isPart2, (int) ei.TOT, (int) ei.Q_TOT, (int) ei.peakValue, (int) ei.TOTstartTime,
           (int) ei.peakTime, (uint32_t) ei.MTWindx, (int) ei.serialNumber);
//  fprintf(outfile, "%u\n", ei.MTWindx);

#endif
//  cout << "Channel " << ch << " event: " << ei << endl;

  bool good = false;
  size_t j;
  
#ifdef ENABLE_DATA_CHECK
  for (j = 0; j < eventList.size (); ++j)
    {
      const eventInfoForArchiving & i = eventList[j];
//     cout << "Considering: " << i << endl;
      if (((ei.MTWindx + ((uint64_t) wraparounds << 32)) % MTW_PERIODS_IN_TEST_PATTERN ==
	   i.MTWindx) && (ei.Q_TOT == i.Q_TOT) && (ei.TOT == i.TOT)
	  && (ei.TOTlimitReached == i.TOTlimitReached)
	  && (ei.TOTstartTime == i.TOTstartTime)
	  && (ei.TOTvalid == i.TOTvalid) && (ei.isPart2 == i.isPart2)
	  && (ei.peakTime == i.peakTime) && (ei.peakValid == i.peakValid)
	  && (ei.peakValue == i.peakValue) && (ei.pileUp == i.pileUp))
	{
//          cout << "Found" << endl;
	  good = true;
	  break;
	}
    }
#endif

#ifndef IS_EEE_MODULE_NO_TRACE
#ifdef ENABLE_DATA_CHECK
if(good)
  last_index = (j + 1) % eventList.size(); 

//  cout << "Serial: " << ei.serialNumber <<endl; 
#ifndef PUT_TIMESTAMP
if (data_check)
  if (ei.serialNumber != lastSerialNumber)
    cout << "Channel " << ch << ": Bad event serial number is " << ei.serialNumber << " should be " << lastSerialNumber << endl;
#endif
#endif
    
  lastSerialNumber = ei.serialNumber+1;

#ifndef PUT_TIMESTAMP
  if(data_check)
  if (!good)
  {
    cout << "Channel " << ch << ": Bad event" << endl;
#ifdef USE_TXTFILES
    fprintf(outfile, "Bad event\n"); 
#endif
  }
  //else
  //  cout << "Channel " << ch <<  ": good event" << endl;
#endif

#if 0
  cout << "Channel " << ch << "MTWindx" << ei.MTWindx << endl;

  uint32_t event_index =
    eventList.back ().if ((last_event_mtw < MTWindx - 10)
			  || (last_event_mtw > MTWindx))
    printf ("Error: %u - %u\n", last_event_mtw, MTWindx);

  last_event_mtw = MTWindx;
#endif
#endif
}

void
DataProcessor::process_neutron (struct u256 *d, timestamp_triple range[2])
{
  struct neutronCounts nc;
  int32_t background_count;

  nc.N_n = u256_get_top_bits_and_cut (d, 8);
  nc.N_qtot = u256_get_top_bits_and_cut (d, 32);
  nc.negative_saturations = u256_get_top_bits_and_cut (d, 8);
  nc.positive_saturations = u256_get_top_bits_and_cut (d, 8);
  background_count =  u256_get_top_bits_and_cut (d, 26);
  if (background_count >= (1 << 25) )
    background_count -= (1 << 26);
  nc.Q_background = background_count;

  int32_t Qtotal = u256_get_top_bits_and_cut (d, 25);
  if (Qtotal >= (1 << 24) )
    Qtotal -= (1 << 25);
  nc.Qtotal = Qtotal;

  uint8_t n = nc.N_n;
  int32_t q_n = nc.N_qtot;
  int8_t negative_saturations = nc.negative_saturations;
  int8_t positive_saturations = nc.positive_saturations;


  int_timestamp st;
  st.MTW = current_window;
  st.sample = 0;
  uint64_t evt_ts = get_evt_ts(st);


  if ((prev_MTWindx > UINT32_MAX / 2)
      && (current_window < UINT32_MAX / 2))
    wraparounds++;

  prev_MTWindx = current_window;

#ifdef ENABLE_DATA_CHECK
  uint32_t window_reduced =
    (current_window + ((uint64_t) (wraparounds) << 32) -
     1) % neutronList.size ();

if(data_check)
{
#ifndef IS_EEE_MODULE_NO_TRACE
if (current_window != last_neutron_window)
  cout << "Missing neutron data: "  << current_window << " " << last_neutron_window << endl;
#endif
if(last_neutron_channel != 0)
{
  last_neutron_channel--;
}
else
{
last_neutron_channel = 5;
last_neutron_window++;
}

#ifndef PUT_TIMESTAMP
  if ((n == neutronList[window_reduced].N_n) &&
      (q_n == neutronList[window_reduced].N_qtot))
    ;				//cout << "Good neutron" << endl;
#ifndef IS_EEE_MODULE_NO_TRACE
  else
    {
      cout << "Bad neutron window_reduced = " << window_reduced <<
	" should be " << (uint32_t) neutronList[window_reduced].N_n << " - "
	<< neutronList[window_reduced].N_qtot << endl;
    }
#endif
#endif
    
}
#endif

#ifdef USE_HDF5
if(enableHDF5)
{
  hdf5_neutronCount di;

  di.evt_timestamp = evt_ts;
  di.MTWindx = current_window;  
  di.q_N = q_n;
  di.n = n;
  di.negative_saturations = negative_saturations;
  di.positive_saturations = positive_saturations;
  di.background_count = background_count;
  di.channel = current_event_channel;
  AIChannels.at(current_event_channel)->hdf5Obj->writeNeutronCountToHdf (di);
}
#endif

  AIChannels.at(current_event_channel)->readNeutronCounter(nc);

#ifdef USE_TXTFILES
 fprintf (outfile, "Channel %d MTW=%u TS=%" PRIu64 " n=%u q_n=%u neg_sat=%u pos_sat=%u bcg_cnt=%d qtot=%d\n",
          current_event_channel, current_window, evt_ts, n, q_n, negative_saturations, positive_saturations, background_count, Qtotal);
#endif


  if (current_event_channel > 0)
    --current_event_channel;
  else
    {
      current_event_channel = 5;
      ++current_window;
    }
}


void
DataProcessor::process_sample (struct u256 *d, uint32_t header_mtw,
                               uint8_t header_sample_idx,
                               timestamp_triple range[2])
{
  if ((prev_MTWindx > UINT32_MAX / 2) && (header_mtw < UINT32_MAX / 2))
    wraparounds++;

  prev_MTWindx = header_mtw;

  int_timestamp st;
  st.MTW = mtw_index;
  st.sample = sample_index;

  uint64_t evt_ts = get_evt_ts(st);

  uint32_t sample_pair = u256_get_top_bits_and_cut (d, 32);
  uint64_t extended_index = header_mtw + ((uint64_t) (wraparounds) << 32);

//  cout << "SampleList.size()" << sampleList.size() << endl;
#ifdef ENABLE_DATA_CHECK
  uint32_t window_reduced = extended_index % (sampleList.size () / 125);
  uint32_t index = 125 * window_reduced + header_sample_idx / 2;
#endif
  //cout << "header_mtw=" << std::hex << header_mtw << " header_sample_idx=" <<
//    std::hex << (uint32_t) header_sample_idx << std::dec << " index=" << index
//    << endl;

//printf("collecting data CB channel %d\r", ch);
AIChannels.at(ch-cb_channel_rawdata)->scope((double)(extended_index * 250 + header_sample_idx), (uint16_t)(sample_pair & 0xffff), (uint16_t)(sample_pair >> 16));

#ifdef USE_HDF5
if(enableHDF5)
{
  hdf5_timestampAndSample di[2];
  
  di[0].evt_timestamp = evt_ts;
  di[0].timestamp = extended_index * 250 + header_sample_idx;
  di[0].sample = sample_pair & 0xffff;

  if (evt_ts) evt_ts += 4;

  di[1].evt_timestamp = evt_ts;
  di[1].timestamp = di[0].timestamp + 1;
  di[1].sample = sample_pair >> 16;
  AIChannels.at(ch-cb_channel_rawdata)->hdf5Obj->writeTimestampAndSampleToHdf (di);
}
#endif
#ifdef USE_TXTFILES
  fprintf (outfile, "%" PRIu64 "\t%" PRIu64 "\t%" PRIu32 "\n", extended_index * 250 + header_sample_idx, evt_ts, sample_pair & 0xffff);
  fprintf (outfile, "%" PRIu64 "\t%" PRIu64 "\t%" PRIu32 "\n", extended_index * 250 + header_sample_idx + 1, evt_ts + 4, sample_pair >> 16);
#endif

#ifdef ENABLE_DATA_CHECK
  const preprocessedInfo & pi = sampleList[index];

//  printf ("%x - %x - %x\n", pi.sample0, pi.sample1, sample_pair);

#ifdef PUT_TIMESTAMP
// test pattern
  uint16_t sample1 = (header_sample_idx << 8) | 0xea;
  uint16_t sample0 = header_mtw & 0xffff;

  uint32_t refdata = (uint32_t) (sample1) << 16 | sample0;

if(data_check)
{
  if (refdata == sample_pair)
    ;//cout << "Good sample" << endl;
  else
    cout << "Channel " << ch << " Bad sample" << endl;
}
#else
// reference data
if(data_check)
{
  if ((((uint32_t) (pi.sample1) << 16) | pi.sample0) == sample_pair)
  {
//    cout << "Good sample" << endl;
  }
#ifndef IS_EEE_MODULE_NO_TRACE
  else
    cout << "Channel " << ch << " Bad sample:"  << std::hex << (((uint32_t) (pi.sample1) << 16) | pi.sample0) << " - " << std::hex << sample_pair  << std::dec  << endl;
#endif
}
#endif
#endif

}



void
DataProcessor::process_statistics (uint8_t * buf, uint32_t data_size,
                                   int buffer_size)
{
  //int ch = cb_channel_periodic;
  u256 d = { 0 };
  uint32_t offset = 0;
  if ((prev_MTWindx > UINT32_MAX / 2)
      && (current_window < UINT32_MAX / 2))
    wraparounds++;

  prev_MTWindx = current_window;

#if 0
  uint32_t window_reduced =
    (current_window + ((uint64_t) (wraparounds[ch]) << 32) -
     1) % neutronList.size ();

  (void)window_reduced; // silence unused variable warning
#endif

  uint8_t info_byte = current_event_channel;

struct pedestalInfo pi[6];

  if (info_byte == 1)
    {
      for (int i = 0; i < 6; ++i)
	{
    pi[i].num_of_samples = u256_get_data_and_shift_int (&d, 25, buf, &offset, buffer_size);
    pi[i].sum_of_samples = u256_get_data_and_shift_int (&d, 41, buf, &offset, buffer_size);
    pi[i].sum_of_squares = u256_get_data_and_shift_int (&d, 57, buf, &offset, buffer_size);
    pi[i].negative_saturations = u256_get_data_and_shift_int (&d, 25, buf, &offset, buffer_size);
    pi[i].positive_saturations = u256_get_data_and_shift_int (&d, 25, buf, &offset, buffer_size);

    AIChannels.at(5-i)->readDetectorSpecificDataInTnom(pi[i]);

#ifdef USE_TXTFILES
	  uint32_t num_of_samples = pi[i].num_of_samples;
	  uint64_t sum_of_samples = pi[i].sum_of_samples;
	  uint64_t sum_of_squares = pi[i].sum_of_squares;
	  uint64_t num_of_negative_saturations = pi[i].negative_saturations;
	  uint64_t num_of_positive_saturations = pi[i].positive_saturations;

	  fprintf (outfile,"Channel %d MTW=%u num_of_samples=%"  PRIu32
		  " sum_of_samples=%"  PRIu64 " sum_of_squares=%"  PRIu64
		  " num_of_negative_saturations=%"  PRIu64
		  " num_of_positive_saturations=%"  PRIu64 " average=%f\n", i,
		  current_window, num_of_samples, sum_of_samples,
		  sum_of_squares, num_of_negative_saturations,
		  num_of_positive_saturations,
		  (double) sum_of_samples / num_of_samples);
#endif
	}
    }
  if (info_byte == 8)
    {
    uint64_t count=0;
    int beam=0;
#ifdef USE_TXTFILES
      fprintf(outfile, "MTW=%" PRIu32" Loss=", current_window);
#endif
      for (int i = 0; i < 6; ++i)
	{
	  int64_t loss_in_window =
	    u256_get_data_and_shift_int (&d, 32, buf, &offset, buffer_size);
#ifdef USE_TXTFILES
	  fprintf(outfile, " loss_in_window:%" PRId64" ", loss_in_window);
#endif

	  int64_t QtotalMax =
	    u256_get_data_and_shift_int (&d, 25, buf, &offset, buffer_size);
	  if (QtotalMax & (1ull << 24))
	      QtotalMax -= (1ull << (25));
#ifdef USE_TXTFILES
	  fprintf(outfile, " QtotalMax:%" PRId64" ", QtotalMax);
#endif

	  int64_t QtotalMin =
	    u256_get_data_and_shift_int (&d, 25, buf, &offset, buffer_size);
	  if (QtotalMin & (1ull << 24))
	      QtotalMin -= (1ull << (25));
#ifdef USE_TXTFILES
	  fprintf(outfile, " QtotalMin:%" PRId64" ", QtotalMin);
#endif

	  int64_t Qtotal_sum =
	    u256_get_data_and_shift_int (&d, 42, buf, &offset, buffer_size);
	  if (Qtotal_sum & (1ull << 41))
	      Qtotal_sum -= (1ull << (42));
#ifdef USE_TXTFILES
	  fprintf(outfile, " Qtotal_sum:%" PRId64" ", Qtotal_sum);
#endif

	  int64_t Qbackground_max =
	    u256_get_data_and_shift_int (&d, 26, buf, &offset, buffer_size);
	  if (Qbackground_max & (1ull << 25))
	      Qbackground_max -= (1ull << (26));
#ifdef USE_TXTFILES
	  fprintf(outfile, " Qbackground_max:%" PRId64" ", Qbackground_max);
#endif

	  int64_t Qbackground_min =
	    u256_get_data_and_shift_int (&d, 26, buf, &offset, buffer_size);
	  if (Qbackground_min & (1ull << 25))
	      Qbackground_min -= (1ull << (26));
#ifdef USE_TXTFILES
	  fprintf(outfile, " Qbackground_min:%" PRId64" ", Qbackground_min);
#endif

	  int64_t Qbackground_sum =
	    u256_get_data_and_shift_int (&d, 43, buf, &offset, buffer_size);
	    if (Qbackground_sum & (1ull << 42))
	      Qbackground_sum -= (1ull << (43));
#ifdef USE_TXTFILES
	  fprintf(outfile, " Qbackground_sum:%" PRId64" ", Qbackground_sum);
#endif

	  uint64_t neutron_max =
	    u256_get_data_and_shift_int (&d, 32, buf, &offset, buffer_size);
#ifdef USE_TXTFILES
	  fprintf(outfile, " neutron_max:%" PRIu64" ", neutron_max);
#endif

	  uint64_t neutron_min =
	    u256_get_data_and_shift_int (&d, 32, buf, &offset, buffer_size);
#ifdef USE_TXTFILES
	  fprintf(outfile, " neutron_min:%" PRIu64" ", neutron_min);
#endif

	  uint64_t neutron_sum =
	    u256_get_data_and_shift_int (&d, 49, buf, &offset, buffer_size);
#ifdef USE_TXTFILES
	  fprintf(outfile, " neutron_sum:%" PRIu64" ", neutron_sum);
#endif

    if(i==5)
	  {
	    count =  u256_get_data_and_shift_int (&d, 17, buf, &offset, buffer_size);
#ifdef USE_TXTFILES
	    fprintf(outfile, " count:%" PRIu64" ", count);
#endif

	    beam = u256_get_data_and_shift_int (&d, 1, buf, &offset, buffer_size);
#ifdef USE_TXTFILES
	    fprintf(outfile, " beam:%d", beam);
#endif
	  }

      accumulatedSummary_t accumulatedLossInTnom;
      
      accumulatedLossInTnom.beam = (bool)(beam!=0);
      accumulatedLossInTnom.count= count; // 71429 us/14 Hz cycle
      accumulatedLossInTnom.neutron_sum= neutron_sum;
      accumulatedLossInTnom.neutron_min= neutron_min;
      accumulatedLossInTnom.neutron_max= neutron_max;
      accumulatedLossInTnom.Qbackground_sum= Qbackground_sum;
      accumulatedLossInTnom.Qbackground_min= Qbackground_min;
      accumulatedLossInTnom.Qbackground_max= Qbackground_max;
      accumulatedLossInTnom.Qtotal_sum= Qtotal_sum;
      accumulatedLossInTnom.Qtotal_min= QtotalMin;
      accumulatedLossInTnom.Qtotal_max= QtotalMax;
      accumulatedLossInTnom.loss_in_window= loss_in_window;

      AIChannels.at(5-i)->readAccumulatedLossInTnom(accumulatedLossInTnom);
	}
#ifdef USE_TXTFILES
	  fprintf(outfile, "\n");
#endif
    }
  if ((info_byte > 1 && info_byte < 8) || (info_byte > 32))
    {
#ifdef USE_TXTFILES
      fprintf(outfile, "MTW=%" PRIu32" Waveform for channel %d ", current_window, info_byte);
#endif
      
//	u256_get_data_and_shift_int (&d, 4, buf, &offset, buffer_size);
	
        uint64_t decimation_factor =  u256_get_data_and_shift_int (&d, 64, buf, &offset, buffer_size);
        uint32_t window_position =  u256_get_data_and_shift_int (&d, 32, buf, &offset, buffer_size);
        uint32_t window_samples =  u256_get_data_and_shift_int (&d, 16, buf, &offset, buffer_size);
        uint32_t words =  u256_get_data_and_shift_int (&d, 16, buf, &offset, buffer_size);

#ifdef USE_TXTFILES
        fprintf(outfile,"decimation_factor=%" PRIu64" window_position=%" PRIu32" window_samples=%" PRIu32, decimation_factor, window_position, window_samples);
        fprintf(outfile," words=%" PRIu32" ",words);

	fprintf(outfile, "Samples ");
#endif

      lossInUserDefinedWindow_t lossInUserDefinedWindow;
      
      lossInUserDefinedWindow.lossDecimation_factor= decimation_factor; // -1
      lossInUserDefinedWindow.lossWindow_position= window_position;
      lossInUserDefinedWindow.lossWindow_samples= window_samples; // divided by 2
      lossInUserDefinedWindow.lossWords=words;

      protectionFunctionOutputs_t protectionFunctionOutputs;

      protectionFunctionOutputs.protectionDecimation_factor= decimation_factor; // -1
      protectionFunctionOutputs.protectionWindow_position= window_position;
      protectionFunctionOutputs.protectionWindow_samples= window_samples; // divided by 2
      protectionFunctionOutputs.protectionWords=words;

      if(info_byte < 8)
      {
        lossInUserDefinedWindow.lossData_avg.reserve(2*(words-1)+1);
        lossInUserDefinedWindow.lossData_max.reserve(2*(words-1)+1);

        for (unsigned i = 0; i < 2*(words-1); ++i)
	      {
	      int64_t data_sum =
	        u256_get_data_and_shift_int (&d, 32, buf, &offset, buffer_size);
	      int64_t data_max =
	        u256_get_data_and_shift_int (&d, 32, buf, &offset, buffer_size);


#ifdef USE_TXTFILES
	        fprintf(outfile,"s=%" PRIi64" m=%" PRIi64" ", data_sum, data_max);
#endif

          if(window_samples != 0)
            lossInUserDefinedWindow.lossData_avg.push_back((double)(data_sum/window_samples)/100);
          else
            lossInUserDefinedWindow.lossData_avg.push_back((double)0);

          lossInUserDefinedWindow.lossData_max.push_back((double)data_max/100);
	      }

        AIChannels.at(info_byte-2)->readLossInUserDefinedWindow(lossInUserDefinedWindow);

      }
      else
      {
        protectionFunctionOutputs.protectionAvg0_max.reserve(2*(words-1)+1);
        protectionFunctionOutputs.protectionAvg1_max.reserve(2*(words-1)+1);
        protectionFunctionOutputs.protectionRelaxation_max.reserve(2*(words-1)+1);
        protectionFunctionOutputs.protectionXY_count.reserve(2*(words-1)+1);

        for (unsigned i = 0; i < (words-1); ++i)
	      {
	        uint64_t avg0_max =
  	        u256_get_data_and_shift_int (&d, 32, buf, &offset, buffer_size);
	        uint64_t avg1_max =
	          u256_get_data_and_shift_int (&d, 32, buf, &offset, buffer_size);
	        uint64_t relaxation_max =
	          u256_get_data_and_shift_int (&d, 32, buf, &offset, buffer_size);
	      uint64_t xy_count =
	        u256_get_data_and_shift_int (&d, 32, buf, &offset, buffer_size);

#ifdef USE_TXTFILES
	      fprintf(outfile,"a0=%" PRIi64" a1=%" PRIi64" r=%" PRIi64" x=%" PRIi64" ", avg0_max, avg1_max,relaxation_max,xy_count);
#endif

          protectionFunctionOutputs.protectionAvg0_max.push_back((double)avg0_max);
          protectionFunctionOutputs.protectionAvg1_max.push_back((double)avg1_max);
          protectionFunctionOutputs.protectionRelaxation_max.push_back((double)relaxation_max);
          protectionFunctionOutputs.protectionXY_count.push_back((double)xy_count);
	      }

        AIChannels.at(info_byte-33)->readProtectionFunctionOutputs(protectionFunctionOutputs);
      }
#ifdef USE_TXTFILES
    fprintf(outfile,"\n");
#endif
	}

    if (info_byte > 8 && info_byte < 33)
    {

/*
  fixed<bits+14,bits+14> sum;
  ap_ufixed<bits*2+14,bits*2+14> sumSquares;
  fixed<bits,bits> min;
  fixed<bits,bits> max;

  typedef EventStatistics<ap_fixed, 26, -(1<<25), (1<<25)-1> Q_TOT_statistics;
  typedef EventStatistics<ap_fixed, 17, -(1<<16), (1<<16)-1> peakValue_statistics ;
  typedef EventStatistics<ap_ufixed, 9, 0, (1<<9)-1> TOT_statistics;
  typedef EventStatistics<ap_ufixed, 9, 0, (1<<9)-1> peakTime_statistics;

  ap_ufixed<14,14> count;

  Q_TOT_statistics Q_TOT;
  peakValue_statistics peakValue;
  TOT_statistics TOT;
  peakTime_statistics peakTime;
*/
     uint64_t s;


     ap_ufixed<9,9> TOT_max = u256_get_data_and_shift_int (&d, 9, buf, &offset, buffer_size);
     ap_ufixed<9,9> TOT_min = u256_get_data_and_shift_int (&d, 9, buf, &offset, buffer_size);
     ap_ufixed<2*9+14,2*9+14> TOT_sumSquares = u256_get_data_and_shift_int (&d, 2*9+14, buf, &offset, buffer_size);
     ap_ufixed<9+14,9+14> TOT_sum = u256_get_data_and_shift_int (&d, 9+14, buf, &offset, buffer_size);

     ap_fixed<17,17> peakValue_max = u256_get_data_and_shift_int (&d, 17, buf, &offset, buffer_size);
     ap_fixed<17,17> peakValue_min = u256_get_data_and_shift_int (&d, 17, buf, &offset, buffer_size);
     ap_ufixed<2*17+14,2*17+14> peakValue_sumSquares = make_ufixed_64_from_uint64(u256_get_data_and_shift_int (&d, 2*17+14, buf, &offset, buffer_size)); //longer than 32 bits
     s = u256_get_data_and_shift_int (&d, 17+14, buf, &offset, buffer_size);
     ap_fixed<17+14,17+14> peakValue_sum = s & ((1ull << (17+14-1))-1); // sign bit correction
     if (s & (1ull << (17+14-1)) )
       peakValue_sum -= (1ull << (17+14-1));

     ap_fixed<26,26> Q_TOT_max = u256_get_data_and_shift_int (&d, 26, buf, &offset, buffer_size);
     ap_fixed<26,26> Q_TOT_min = u256_get_data_and_shift_int (&d, 26, buf, &offset, buffer_size);

     ap_ufixed<2*26+14,2*26+14> Q_TOT_sumSquares = u256_get_data_and_shift_int (&d, 32, buf, &offset, buffer_size);
     Q_TOT_sumSquares <<= 32; // longer than 32 bits - shift data and call function three times
     Q_TOT_sumSquares |= u256_get_data_and_shift_int (&d, 32, buf, &offset, buffer_size);
     Q_TOT_sumSquares <<= 2;
     s = u256_get_data_and_shift_int (&d, 2*26+14 - 64, buf, &offset, buffer_size);
     Q_TOT_sumSquares |= ap_ufixed<2*26+14,2*26+14> (s);
     s = u256_get_data_and_shift_int (&d, 26+14, buf, &offset, buffer_size);
     
     ap_fixed<26+14,26+14> Q_TOT_sum = make_ufixed_64_from_uint64((s & ((1ull << (26+14-1))-1)));
     if (s & (1ull << (26+14-1)) )
       Q_TOT_sum -= make_ufixed_64_from_uint64(1ull << (26+14-1));

     ap_ufixed<14,14> count = u256_get_data_and_shift_int (&d, 14, buf, &offset, buffer_size);


     const int b_ssqtc2 = 2*9+14+14;
     const int b_sqs2 = 2*(9+14); 
     double TOT_average = double(TOT_sum) / double(count);
     double TOT_rms = sqrt(double(ap_ufixed<b_ssqtc2,b_ssqtc2>(TOT_sumSquares)*count 
                           - ap_ufixed<b_sqs2,b_sqs2>(TOT_sum) *  ap_ufixed<b_sqs2,b_sqs2>(TOT_sum)) / double(ap_ufixed<28,28>(count)*ap_ufixed<28,28>(count)));


     const int b_ssqtc3 = 2*17+14+14;
     const int b_sqs3 = 2*(17+14); 
     double peakValue_average = double(peakValue_sum) / double(count);
     double peakValue_rms = sqrt(double(ap_ufixed<b_ssqtc3,b_ssqtc3>(peakValue_sumSquares)*count 
                           - ap_fixed<b_sqs3,b_sqs3>(peakValue_sum) *  ap_fixed<b_sqs3,b_sqs3>(peakValue_sum)) / double(ap_ufixed<28,28>(count)*ap_ufixed<28,28>(count)));


     const int b_ssqtc4 = 2*26+14+14;
     const int b_sqs4 = 2*(26+14); 
     double Q_TOT_average = double(Q_TOT_sum) / double(count);
     double Q_TOT_rms = sqrt(double(ap_ufixed<b_ssqtc4,b_ssqtc4>(Q_TOT_sumSquares)*count 
                           - ap_fixed<b_sqs4,b_sqs4>(Q_TOT_sum) *  ap_fixed<b_sqs4,b_sqs4>(Q_TOT_sum)) / double(ap_ufixed<28,28>(count)*ap_ufixed<28,28>(count)));


#ifdef USE_TXTFILES
     fprintf(outfile, "MTW=%" PRIu32" info=%d", current_window, int(info_byte));
#endif
     int channel = (info_byte - 9) % 6;
     int type = (info_byte-9) / 6;

  struct eventStatistics_t es;

    es.TOTmin = (uint32_t)TOT_min;
    es.TOTmax = (uint32_t)TOT_max;
    es.TOTaverage = TOT_average;
    es.TOTrms = TOT_rms;
    es.peakValueMin = (int32_t)peakValue_min;
    es.peakValueMax = (int32_t)peakValue_max;
    es.peakValueAverage = peakValue_average;
    es.peakValueRms = peakValue_rms;
    es.Q_TOTmin = (int32_t)Q_TOT_min;
    es.Q_TOTmax = (int32_t)Q_TOT_max;
    es.Q_TOTaverage = Q_TOT_average;
    es.Q_TOTrms = Q_TOT_rms;
    es.eventCount = count;
    AIChannels.at(channel)->readEventStatisticsInTcurr(es, type);
     
#ifdef USE_TXTFILES
    const char* types[4] = {"single","pileup","all","background"}; // Obsolete
     fprintf(outfile, " channel=%d %s count=%u ", channel, types[type], unsigned(count));

    fprintf(outfile, "TOT_min=%" PRIu32" TOT_max=%" PRIu32" TOT_average=%f TOT_rms=%f ", (uint32_t)TOT_min, (uint32_t)TOT_max, TOT_average, TOT_rms);
    fprintf(outfile, "peakValue_min=%" PRIi32" peakValue_max=%" PRIi32" peakValue_average=%f peakValue_rms=%f ", (int32_t)peakValue_min, (int32_t)peakValue_max, peakValue_average, peakValue_rms);
    fprintf(outfile, "Q_TOT_min=%" PRIi32" Q_TOT_max=%" PRIi32" Q_TOT_average=%f Q_TOT_rms=%f ", (int32_t)Q_TOT_min, (int32_t)Q_TOT_max, Q_TOT_average, Q_TOT_rms);

    fprintf(outfile, "\n");
#endif
    }    

}



void
DataProcessor::process_stream_data_item (struct u256 *d, uint32_t header_mtw,
                                         uint8_t header_sample_idx,
                                         timestamp_triple * range)
{
//  d->bits -= data_item_sizes[ch];
  if (ch >= cb_channel_rawdata)
    process_sample (d, header_mtw, header_sample_idx, range);
  else if (ch == cb_channel_neutrons)
    process_neutron (d, range);
  else    
    process_event (d, range);
}



void DataProcessor::processOfflineDataInterleaved (uint16_t timeout_ms)
{
  size_t i=0;
  std::vector<varianttype> variant_v;
  size_t variant_v_size;
  size_t old_block_index = -1;
  size_t offset = 0;
//  timestamp_triple range[2];
//  int index_range[2];
  size_t copied_items;
  double t0, t1;
  size_t bytes_to_read=16;
  uint32_t samples_in_frame=0;
  uint32_t data_bits_in_frame=0;
  uint32_t crc=0;
  uint32_t good_frames = 0;  
  
  if (ch != cb_channel_periodic)
    printf("Starting processOfflineDataInterleaved(%d) task\n", ch);
  t0 = rTime ();

  block_index = 0;

  variant_v_size = buffers_timestamped.items();
  variant_v.resize(variant_v_size);
  i = 0;

  if(variant_v_size == 0)
    return;

  while ( (i < variant_v_size) && (!exit_processData) && (!exit_loop))
  {
    buffers_timestamped.get_data(&variant_v[i], 1, 1, copied_items, 0);
    // if ( variant_v[i].holds_alternative(TIMESTAMP) )
    //   printf("Timestamp found in buffers_timestamped\n");
    i++;
  }
  input_ptr = &variant_v;

  if(ch != cb_channel_periodic)
  {
    try {
      timestamp_sanity_check (variant_v);
    }
    catch (const char *msg)
    {
      printf ("channel %d: %s, disabling timestamping\n", ch, msg);
      real_timestamps = false;
    }

    for(auto& i: variant_v)
    {
        if (i.holds_alternative(TIMESTAMP))
        {
          const timestamp_triple& tt = i.get_TIMESTAMP();
#ifdef USE_HDF5
          hdf5_triggerEventInfo di;
          di.int_timestamp = int_to_u64(tt.i);
          di.evt_timestamp = evt_to_u64(tt.e);
          di.channel = ch;
          if(ch >= cb_channel_rawdata)
            AIChannels.at(ch-cb_channel_rawdata)->hdf5Obj->writeTriggerEventInfoToHdf (di);
#endif
          //printf("Times in the buffer (since epoch in sec) = %ld.%ld\n", tt.c.tv_sec, tt.c.tv_usec);
        }
    }
  }

    while ( block_index < variant_v.size() && (!exit_processData) && (!exit_loop))
    {
      uint8_t bytesArray[16];
      uint32_t *words;

      if(bytes_to_read < 16)
      {
        for(size_t j=0; j < (16-bytes_to_read); j++)
          bytesArray[j]=bytesArray[j+bytes_to_read];
      }

      if (!get_data_from_buffer (variant_v, &bytesArray[16-bytes_to_read], block_index, offset, bytes_to_read))
        break;

      words = (uint32_t*)bytesArray;

      if (old_block_index != block_index)
        newblock = true;        //FIXME: probably the condition for that is not right
      old_block_index = block_index;

      // if(newblock)
      //   printf("Analyse %d frame\n", block_index);

      uint32_t word0 = bswap_32 (words[0]);
      uint32_t word1 = bswap_32 (words[1]);
      uint32_t word2 = bswap_32 (words[2]);
      uint32_t word3 = bswap_32 (words[3]);

      uint8_t *lb = bytesArray;

      switch (sis)
        {
        case SIS_SEARCHING:
          if ((word0 == SOF_WORD_0) && (word1 == SOF_WORD_1))
            {
              mtw_index = word2;
              sample_index = word3 >> 24;
              samples_in_frame = (word3 & 0x0000ffff);
              uint8_t frame_info = (word3 >> 16) & 0xff;
              current_event_channel = frame_info & 0x7f;

              if (ch == cb_channel_neutrons)
                {
                  if (current_event_channel == (0xee & 0x7f))
                    printf ("Error - info field in header set to 'EE'\n");
                }

              if (frame_info & 0x80)
                printf ("Channel %d: frame fifo overflow at MTW=%" PRIu32
                        "\n", ch, mtw_index);

              current_window = mtw_index;
#ifdef DEBUG_RAW_VALUES
              printf ("MTW=%u sample=%u samples_in_frame=%u\n", mtw_index,
                      sample_index, samples_in_frame);
#endif

//            printf("Event header: %d\n", current_event_channel[ch]);
              data_bits_in_frame = samples_in_frame * data_item_sizes[ch];
//                    printf("Samples in frame: %u\n",samples_in_frame[ch]);

              if (samples_in_frame > 0)
                sis = SIS_DATA;
              else
                sis = SIS_CRC;

              data.bits = 0;

              if (compute_crc)
                {
                  crc = 0xffffffff;
                  crc = crc32 (crc, lb);
                }
              data_size = 0;
              bytes_to_read = 16;
            }
          else
          {
            // printf ("&");
            // printf ("Channel:%d   Data:%08x%08x%08x%08x\n", ch, word0, word1, word2, word3);
            // printf("waiting %08x%08x\n", SOF_WORD_0, SOF_WORD_1);
            
            // Start pattern are not found. In order to resynchronize the stream,
            // read only one new byte in the next 4 words extraction
            bytes_to_read = 1;
          }
          break;
        case SIS_DATA:
          if (compute_crc)
          {
            crc = crc32 (crc, lb);
          }

          if (data_bits_in_frame > 128)
          {
            data_bits_in_frame -= 128;
          }
          else
            sis = SIS_CRC;

          if (data_size + 16 > MAXFRAMESIZE)
          {
            printf ("Error - frame buffer overflow for CB%d\n", ch);
            sis = SIS_SEARCHING;
            break;
            //exit (EXIT_FAILURE);
          }
          memcpy (frame_buffer + data_size, lb, 16);
          data_size += 16;
//        printf ("Channel:%d   Data:%08x%08x%08x%08x\n", ch, word0,
//                word1, word2, word3);
          break;
        case SIS_CRC:
//        printf ("Channel:%d   Data:%08x%08x%08x%08x\n", ch, word0,
//                word1, word2, word3);
          {
            int good = 1;
            if (compute_crc)
              {
                if (crc == word0)
                  {
//                  printf ("CRC OK\n");
                  }
                else
                  {
                    good = 0;
                    printf ("CH%d: CRC NOT OK, block %d\n", ch, (uint32_t)block_index);
                  }
              }
            if ((word1 == EOF_WORD_1) && (word2 == EOF_WORD_2)
                && (word3 == EOF_WORD_3))
              {
//              printf ("EOF marker found\n");
              }
            else
              {
                printf ("CH%d: EOF marker not found, block %d\n", ch, (uint32_t)block_index);
                good = 0;
              }
            if (!good)
              {
//              printf ("Good frames: %u\n", good_frames);
                good_frames = 0;
              }
            else
              {
                good_frames++;
                if (ch != cb_channel_periodic)
                  {
                    uint32_t bytes_processed = 0;
                    uint32_t samples_processed = 0;
              //cout << "Data size CB" << ch << ": " << data_size << " , samples_in_frame= " << samples_in_frame << endl;
                    while (bytes_processed < data_size)
                      {
                        u256_shift_in (&data, frame_buffer + bytes_processed);
                        while ((data.bits >= data_item_sizes[ch])
                               && (samples_processed < samples_in_frame))
                          {
                            // Possible only when "Data On Demand CCS Preview" is set and CB >= 8.
                            // Check if a trig occurs during raw data displaying
                            if(ch >= cb_channel_rawdata)
                            {
                              struct timespec timeout;
                              int result;

                              // Wait for resuming the preview acquisition by pushing again the "Start DOD collecting for all activated CB channels"
                              // It also stops waiting when user pushes "break DOD collecting", or exit_loop or exit_processData
                              pthread_mutex_lock (&AIChannels.at(ch-cb_channel_rawdata)->readNonPeriodicCBchannel.mutexRetrieveData);
                              clock_gettime(CLOCK_REALTIME, &timeout);
                              // timeout = 1 second
                              timeout.tv_sec += 1;
                              result = 0;
                              // Wait with Timeout for resuming the raw data retrieving and displaying again after a trig
                              while ((AIChannels.at(ch-cb_channel_rawdata)->readNonPeriodicCBchannel.retievePause) && (!exit_loop) && (!exit_processData))
                              {
                                result = pthread_cond_timedwait (&AIChannels.at(ch-cb_channel_rawdata)->readNonPeriodicCBchannel.condRetrieveData, &AIChannels.at(ch-cb_channel_rawdata)->readNonPeriodicCBchannel.mutexRetrieveData, &timeout);
                                if (result == ETIMEDOUT)
                                {
                                    clock_gettime(CLOCK_REALTIME, &timeout);
                                    timeout.tv_sec += 1;
                                }        
                              }
                              pthread_mutex_unlock (&AIChannels.at(ch-cb_channel_rawdata)->readNonPeriodicCBchannel.mutexRetrieveData);
                            }

                            process_stream_data_item (&data,
                                                      mtw_index,
                                                      sample_index, range);
                            ++samples_processed;
                            sample_index += 2;
                            if (sample_index >= 250)
                              {
                                sample_index -= 250;
                                ++mtw_index;
                              }
                          }
                        bytes_processed += 16;
                      }
                  }
                else
                  process_statistics (frame_buffer, data_size, data_size);
              }
            sis = SIS_SEARCHING;
          }
          break;
        }; // end of switch(sis)

    // User asks for breaking the DOD request.
    if((ch != cb_channel_periodic) && (!get_fillFile()))
      break;

    t1 = rTime ();
    if( (timeout_ms) && (((t1 - t0) / 1000) > timeout_ms) )
    {
      printf("timeout in processOfflineDataInterleaved() for CH%d\n", ch);
      break;
    }

#ifdef DEBUG_RAW_VALUES
      printf ("Channel:%d   Data:%08x%08x%08x%08x\n", ch, word0, word1, word2, word3);
#endif    
   } //end of while ( block_index < variant_v.size() && (!exit_processData) && (!exit_loop))

    t1 = rTime ();
    //printf ("processOfflineDataInterleaved channel %d: frame analysis = %.3f ms\n", ch, (t1 - t0) / 1000);  
    if (ch != cb_channel_periodic)
      printf("End of processOfflineDataInterleaved(%d) task\n", ch);
}


// For periodic CB 7
int DataProcessor::processOnlineData (uint32_t timeout_ms)
{
  //double t0, t1;
  uint32_t samples_in_frame=0;
  uint32_t data_bits_in_frame=0;
  uint32_t crc=0;
  uint32_t good_frames = 0;
  uint32_t frameAnalysisCount = 0;
  uint32_t frameAnalysisBuffer[4];
  
  if (ch != cb_channel_periodic)
    printf("Starting processOnlineData(%d) task\n", ch);
  //t0 = rTime ();

  size_t minimumSize = 16 ;
  size_t copiedBytes = 0 ;

    if (ch != cb_channel_periodic)
    {
      printf("Error: The processOnlineData() function must be dedicated to the CB 7\n");
      return -1;
    }

    do
    {
      minimumSize = 16-frameAnalysisCount;
      buffers.get_data( &((uint8_t*)(frameAnalysisBuffer))[frameAnalysisCount], minimumSize, 16, copiedBytes, timeout_ms);
      frameAnalysisCount += copiedBytes;

//      printf("frameAnalysisCount = %d, copiedBytes = %d\n\r", frameAnalysisCount, copiedBytes);
      
      if ( frameAnalysisCount == 16 )
      {
      uint32_t word0 = bswap_32 (frameAnalysisBuffer[0]);
      uint32_t word1 = bswap_32 (frameAnalysisBuffer[1]);
      uint32_t word2 = bswap_32 (frameAnalysisBuffer[2]);
      uint32_t word3 = bswap_32 (frameAnalysisBuffer[3]);

      uint8_t *lb = (uint8_t *) frameAnalysisBuffer;

      switch (sis)
        {
        case SIS_SEARCHING:
          if ((word0 == SOF_WORD_0) && (word1 == SOF_WORD_1))
            {
              mtw_index = word2;
              sample_index = word3 >> 24;
              samples_in_frame = (word3 & 0x0000ffff);
              uint8_t frame_info = (word3 >> 16) & 0xff;
              current_event_channel = frame_info & 0x7f;

              if (ch == cb_channel_neutrons)
                {
                  if (current_event_channel == (0xee & 0x7f))
                    printf ("Error - info field in header set to 'EE'\n");
                }

              if (frame_info & 0x80)
                printf ("Channel %d: frame fifo overflow at MTW=%" PRIu32
                        "\n", ch, mtw_index);

              current_window = mtw_index;
#ifdef DEBUG_RAW_VALUES
              printf ("MTW=%u sample=%u samples_in_frame=%u\n", mtw_index,
                      sample_index, samples_in_frame);
#endif

//            printf("Event header: %d\n", current_event_channel[ch]);
              data_bits_in_frame = samples_in_frame * data_item_sizes[ch];
//                    printf("Samples in frame: %u\n",samples_in_frame[ch]);

              if (samples_in_frame > 0)
                sis = SIS_DATA;
              else
                sis = SIS_CRC;

              data.bits = 0;

              if (compute_crc)
                {
                  crc = 0xffffffff;
                  crc = crc32 (crc, lb);
                }
              data_size = 0;
            }
          // else
          //   printf ("&");
          break;
        case SIS_DATA:
          if (compute_crc)
          {
            crc = crc32 (crc, lb);
          }

          if (data_bits_in_frame > 128)
          {
            data_bits_in_frame -= 128;
          }
          else
            sis = SIS_CRC;

          if (data_size + 16 > MAXFRAMESIZE)
          {
            printf ("Error - frame buffer overflow for CB%d\n", ch);
            sis = SIS_SEARCHING;
            break;
            //exit (EXIT_FAILURE);
          }
          memcpy (frame_buffer + data_size, lb, 16);
          data_size += 16;
//        printf ("Channel:%d   Data:%08x%08x%08x%08x\n", ch, word0,
//                word1, word2, word3);
          break;
        case SIS_CRC:
//        printf ("Channel:%d   Data:%08x%08x%08x%08x\n", ch, word0,
//                word1, word2, word3);
          {
            int good = 1;
            if (compute_crc)
              {
                if (crc == word0)
                  {
//                  printf ("CRC OK\n");
                  }
                else
                  {
                    good = 0;
                    printf ("CH%d: CRC NOT OK, block %d\n", ch, (uint32_t)block_index);
                  }
              }
            if ((word1 == EOF_WORD_1) && (word2 == EOF_WORD_2)
                && (word3 == EOF_WORD_3))
              {
//              printf ("EOF marker found\n");
              }
            else
              {
                printf ("CH%d: EOF marker not found, block %d\n", ch, (uint32_t)block_index);
                good = 0;
              }
            if (!good)
              {
//              printf ("Good frames: %u\n", good_frames);
                good_frames = 0;
              }
            else
              {
                good_frames++;
                process_statistics (frame_buffer, data_size, data_size);
              }
            sis = SIS_SEARCHING;
          }
          break;
        }; // end of switch(sis)
        frameAnalysisCount = 0;
        // if(sis != SIS_SEARCHING)
        // {
        //   frameAnalysisCount = 0;
        // }
        // else
        // {
        //   for(int i =0; i<15; i++)
        //     ((uint8_t*)(frameAnalysisBuffer))[i] = ((uint8_t*)(frameAnalysisBuffer))[i+1];
        //   frameAnalysisCount = 15;
        // }
    }; //end of if ( frameAnalysisCount == 16 )

#ifdef DEBUG_RAW_VALUES
      printf ("Channel:%d   Data:%08x%08x%08x%08x\n", ch, word0, word1, word2, word3);
#endif    
   } while ( (copiedBytes ==  minimumSize) && (!exit_processData) && (!exit_loop)) ;

//    t1 = rTime ();
//    printf("Process time in processOnlineData() for CH7 = %f ms\n", ((t1 - t0) / 1000));

   return 0;
}


uint64_t DataProcessor::get_evt_ts (int_timestamp st)
{
  uint64_t evt_ts;
  if (real_timestamps)
    {
     try {
        if (newblock)
        {
            find_matching_timestamp_range(*input_ptr, st, block_index, range, index_range);
            newblock = false;
        }
        else
            find_next_matching_timestamp_range_same_block
                                    (*input_ptr, st, block_index, range, index_range);
        }
        catch (const char *msg)
        {
            printf("channel %d: %s, disabling timestamping\n",ch, msg);
            real_timestamps = false;
            return 0;
        }
      try {
            evt_ts = interpolate_evt_timestamp (st, range);
          }
      catch (const char *msg)
      {
          printf ("channel %d: %s, setting timestamp to 0\n", ch, msg);
          evt_ts = 0;
      }
    }
  else
    evt_ts = 0;
  return evt_ts;
}