
# m-epics-path
local_nblmapp_PATH = "/home/ceauser/e3-3.15.5/e3-nblmapp/nblmapp/misc/"    # my computer, testing purpose
server_nblmapp_PATH = "/home/ceauser/e3-3.15.5/e3-nblmapp/nblmapp/opi/Scripts"        # on the server

# file name (input for save, output for restore)
fileName =  [
                local_nblmapp_PATH + "config_nblmapp/nblm_config0.txt",
                local_nblmapp_PATH + "config_nblmapp/nblm_config1.txt",
                local_nblmapp_PATH + "config_nblmapp/nblm_config2.txt",
                local_nblmapp_PATH + "config_nblmapp/nblm_config3.txt",
                local_nblmapp_PATH + "config_nblmapp/nblm_config4.txt",
                local_nblmapp_PATH + "config_nblmapp/nblm_config5.txt",
            ]

# defines PV MACROs
PREFIX          = "PBI-nBLM00" 
DEVICE          = "Ctrl-AMC-130"         
CHANNEL         =   [
                        "CH0-",
                        "CH1-",
                        "CH2-",
                        "CH3-",
                        "CH4-",
                        "CH5-",
                    ]

PVlistTosaveAndRestore =    [   # neutron detection configuration
                                "evtThr",
                                "evtThr2",
                                "QTOT_n_set",
                                "n_AmpMin",
                                "n_TOTMinIdx",
                                "pedestal",
                                "PU_TOTStart",
                                "pdstlWStart",
                                "pdstlWSize",
                                "WStartLoss",
                                "WSizeLoss",
                                "n_Cnt",
                                "filtr0Size",
                                "filtr1Size",
                                "xyThr",
                                "ma0Thr",
                                "ma1Thr",
                                "avPulseThr",
                                "expThr",
                                "filterxThr",
                                "filterSize",
                                "beamPrmitLUT",
                                "WWaveStart",
                                "WWaveSize",
                                "WWaveDecim",
                                "channelSrc",
                                "pdstlNoEvt",
                                "lambda",
                            ]

PVgloballistToCopy =        [   # parameters apply to the IFC1410
                                "decimStart",
                                "decimStop",
                                "T1period",
                                "T2period",
                                "T3period",
                                "T4period",
                                "T5period",
                                "T6period",
                                "T7period",
                                "dispFreq",
                            ]
                            
