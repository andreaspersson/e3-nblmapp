importPackage(Packages.org.csstudio.opibuilder.scriptUtil);
 
var nblm_prefix = PVUtil.getString(pvArray[0]);
var nblm_device = PVUtil.getString(pvArray[1]);
//var nblm_channel = PVUtil.getString(pvArray[2]);
 
// create a new macro structure and insert
// the nblm macro with the value of the PV
var nblm_macros = DataUtil.createMacrosInput(true);
nblm_macros.put("PREFIX", nblm_prefix);
nblm_macros.put("DEVICE", nblm_device);
//nblm_macros.put("CHANNEL", nblm_channel);

//var pvName = nblm_prefix + ":" + nblm_device + "-NBLM_INPUTMODE";
//PVUtil.writePV(pvName, nblm_channel);

widget.setPropertyValue("macros", nblm_macros);

// reload the embedded OPI
widget.setPropertyValue("opi_file", "");
widget.setPropertyValue("opi_file", "nblm_daq_x.opi");
