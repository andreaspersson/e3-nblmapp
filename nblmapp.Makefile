
## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile
include $(E3_REQUIRE_CONFIG)/DECOUPLE_FLAGS


REQUIRED += asyn nds3epics adsupport nds3 tsc

ifneq ($(strip $(ASYN_DEP_VERSION)),)
asyn_VERSION=$(ASYN_DEP_VERSION)
endif

ifneq ($(strip $(NDS3_DEP_VERSION)),)
nds3_VERSION=$(NDS3_DEP_VERSION)
endif

ifneq ($(strip $(NDS3EPICS_DEP_VERSION)),)
nds3epics_VERSION=$(NDS3EPICS_DEP_VERSION)
endif

ifneq ($(strip $(TSC_DEP_VERSION)),)
tsc_VERSION=$(TSC_DEP_VERSION)
endif

ifneq ($(strip $(ADSUPPORT_DEP_VERSION)),)
adsupport_VERSION=$(ADSUPPORT_DEP_VERSION)
endif


APPSRC := src
APPDB := db

USR_INCLUDES += -I$(where_am_I)$(APPSRC)
USR_INCLUDES += -I$(where_am_I)$(APPSRC)/include
USR_CXXFLAGS += -std=c++1y -DIS_EEE_MODULE -DIS_EEE_MODULE_NO_TRACE -fpermissive

SOURCES += $(wildcard $(APPSRC)/*.cpp)
SOURCES += $(wildcard $(APPSRC)/*.c)

DBDS += $(APPSRC)/nblmapp_asub.dbd
DBDS += $(APPSRC)/IFC14AIChannelGroup.dbd


SUBS = $(wildcard $(APPDB)/*.substitutions)
TEMPLATES += $(wildcard $(APPDB)/*.db)
SCRIPTS += $(wildcard ../iocsh/*.iocsh)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)


.PHONY: db
db: $(SUBS) $(TMPS)

.PHONY: $(SUBS)
$(SUBS):
	@printf "Inflating database ... %44s >>> %40s \n" "$@" "$(basename $(@)).db"
	@rm -f  $(basename $(@)).db.d  $(basename $(@)).db
	@$(MSI) -D $(USR_DBFLAGS) -o $(basename $(@)).db -S $@  > $(basename $(@)).db.d
	@$(MSI)    $(USR_DBFLAGS) -o $(basename $(@)).db -S $@

.PHONY: $(SUBS)
$(TMPS):
	@printf "Inflating database ... %44s >>> %40s \n" "$@" "$(basename $(@)).db"
	@rm -f  $(basename $(@)).db.d  $(basename $(@)).db
	@$(MSI) -D $(USR_DBFLAGS) -o $(basename $(@)).db $@  > $(basename $(@)).db.d
	@$(MSI)    $(USR_DBFLAGS) -o $(basename $(@)).db $@

.PHONY: vlibs
vlibs:
