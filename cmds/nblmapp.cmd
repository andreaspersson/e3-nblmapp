################################################
# IOC running command on the IFC1410 board     #
# cmds/nblmapp.cmd                             #
################################################

require nblmapp,master

epicsEnvSet(TOP,                "$(E3_CMD_TOP)/..")

epicsEnvSet(ENGINEER,		"anderslindh <anders.lindholsson@ess.eu>")
epicsEnvSet(LOCATION,		"PBI labs")

epicsEnvSet(PREFIX,             "$(PREFIX=PBI-nBLM00)")
epicsEnvSet(DEVICE,             "$(DEVICE=Ctrl-AMC-)")
# The tens of AMC index indicates the AMC slot in the MTCA.
# For managing several AMC board in the same MTCA:
# write the AMC index separated with an underscore character
# (ex: 110_120 for managing AMC board in slot 1 and 2)
epicsEnvSet(AMC_IDX,            "$(AMC_IDX=130)")

# Constant definitions
epicsEnvSet(TRIG0_PV,           "$(TRIG0_PV=MTCA-EVR:EvtECnt-I.TIME)")
epicsEnvSet(TIMESTAMP,          "$(TIMESTAMP=MTCA-EVR:Time-I.TIME)")
# Set maximum number of samples: SCOPE_RAW_DATA_SAMPLES_MAX for the scope in the code
epicsEnvSet(NELM, 		10000)

epicsEnvSet(EPICS_CA_MAX_ARRAY_BYTES, 400000000)

# ################## acquisition  ##############################
iocshLoad("../iocsh/daq.iocsh",	"PREFIX=$(PREFIX), IDX=$(AMC_IDX), TIMESTAMP=$(TIMESTAMP), TRIG0_PV=$(TRIG0_PV), NELM=$(NELM)")

iocInit
dbl > "PVs.list"


