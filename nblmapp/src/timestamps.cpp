/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by DMCS for the European Spallation Source ERIC
 * Author: Grzegorz Jablonski
 * 		   DMCS, Lodz, Poland
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/types.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <errno.h>
#include <getopt.h>
#include <stdint.h>
#include <pthread.h>
#include <inttypes.h>
#include <signal.h>
#include <float.h>
#include <thread>
#include <mutex>
#include "u256.h"
#include "crc32.h"

#include "buffers.h"

#include "data_source.h"
#include "evdet.h"

#include "hdf5_interface.h"
#include <iostream>
using namespace std;

#include "circular_buffer_blocking.h"
#include "interleaver_thread.h"
#include "data_processor.h"

const uint64_t int_wraparound = (0x100000000 * 250);

uint64_t safe_int_subtract(uint64_t v1, uint64_t v2)
{
  return ((v1 - v2) + int_wraparound) % int_wraparound;
}

uint64_t int_to_u64(int_timestamp st)
{
  return uint64_t(st.MTW)*250 + st.sample;
}

uint64_t evt_to_u64(evt_timestamp st)
{
  return uint64_t(st.seconds)*1000000000 + st.nanoseconds;
}


bool is_lower_equal_in_quarter_range(uint64_t first, uint64_t second)
{
// wraparoud possible and not harmful
  return safe_int_subtract(second,first)  < (uint64_t)0x40000000 * 250;
}

void timestamp_sanity_check(std::vector<varianttype>& input)
{
  int count = 0;
  uint64_t old_int_timestamp = 0;
  uint64_t old_evt_timestamp = 0;
  double minslope=DBL_MAX;
  double maxslope=DBL_MIN;
  uint64_t mindeltax=ULLONG_MAX;
  uint64_t maxdeltax=0;
  uint64_t mindeltay=ULLONG_MAX;
  uint64_t maxdeltay=0;
  
  for (int i=0;i<input.size();++i)
    if (input[i].holds_alternative(TIMESTAMP))
    {
//      printf("Int timestamp: %d-%d\n", input[i].get<timestamp_triple>().i.MTW,input[i].get<timestamp_triple>().i.sample); 
      const timestamp_triple& tt=input[i].get_TIMESTAMP();

      uint64_t new_int_timestamp = int_to_u64(tt.i);
      uint64_t new_evt_timestamp = evt_to_u64(tt.e);
      
      uint64_t deltax = safe_int_subtract(new_int_timestamp,old_int_timestamp);
      uint64_t deltay = new_evt_timestamp - old_evt_timestamp;
      
//      printf("int=%" PRIu64 " evt=%" PRIu64 "\n", new_int_timestamp, new_evt_timestamp);
      
      if (count != 0)
      {
        double slope = double(deltay)/deltax/4.0;
        //printf("deltax=%" PRIu64 " deltay=%" PRIu64 " slope=%.16g\n", deltax, deltay, slope);
        mindeltax=std::min(deltax,mindeltax);
        maxdeltax=std::max(deltax,maxdeltax);
        mindeltay=std::min(deltay,mindeltay);
        maxdeltay=std::max(deltay,maxdeltay);
        
        minslope=std::min(slope,minslope);
        maxslope=std::max(slope,maxslope);
      } 
      
      if (old_evt_timestamp >= new_evt_timestamp)
      {
        throw("EVR timestamps not in ascending order\n");
      }
      
      old_int_timestamp = new_int_timestamp;
      old_evt_timestamp = new_evt_timestamp;
      ++count;
    }
    
  if(count == 0)
    throw("found 0 timestamps\n");

  for (int i=0;i<input.size();++i)
    if (input[i].holds_alternative(TIMESTAMP))
      printf("+");
    else
      printf("O");


  printf("\nTimestamp sanity check: found %d timestamps out of %lu records,"
         " minslope=%.10f, maxslope=%.10f, reldelta=%.3g, mindeltax=%" PRIu64 " maxdeltax=%" PRIu64 
         " mindeltay=%" PRIu64 " maxdeltay=%" PRIu64 "\n",
         count, input.size(), minslope, maxslope, (maxslope-minslope)/maxslope, mindeltax, maxdeltax, mindeltay, maxdeltay);
  //TODO monotonicity etc...
  
}


uint64_t interpolate_evt_timestamp(int_timestamp st, timestamp_triple range[2])
{
  uint64_t int_refval = int_to_u64(st);
  uint64_t lower_int_refval = int_to_u64(range[0].i);
  uint64_t upper_int_refval = int_to_u64(range[1].i);
  uint64_t lower_evt_refval = evt_to_u64(range[0].e);
  uint64_t upper_evt_refval = evt_to_u64(range[1].e);

  if(!is_lower_equal_in_quarter_range(lower_int_refval, upper_int_refval))
    throw("Gap too large during interpolation");
  if(!(lower_evt_refval <= upper_evt_refval))
    throw("Invalid EVR timestamps ordering in interpolation");

  uint64_t xrange = safe_int_subtract(upper_int_refval,lower_int_refval);
  uint64_t yrange = upper_evt_refval - lower_evt_refval;

  if (xrange==0)
  {
    cout << "Zero range in interpolation" << endl;
    if (int_refval != lower_int_refval)
      throw("Zero range in interpolation and timestamp not in range");
    return lower_evt_refval;
  }

  if (is_lower_equal_in_quarter_range(int_refval, lower_int_refval)) // extrapolate left
    return lower_evt_refval - safe_int_subtract(lower_int_refval,int_refval) * yrange / xrange;
  else
  if (is_lower_equal_in_quarter_range(upper_int_refval, int_refval)) // extrapolate right
    return upper_evt_refval + safe_int_subtract(int_refval,upper_int_refval) * yrange/xrange;
  else
  if (is_lower_equal_in_quarter_range(lower_int_refval, int_refval) && is_lower_equal_in_quarter_range(int_refval, upper_int_refval))
    return lower_evt_refval + safe_int_subtract(int_refval,lower_int_refval) * yrange/xrange;
  else
    throw("Wrong interpolation range");
}

void find_next_matching_timestamp_range_same_block(std::vector<varianttype>& input, int_timestamp st, size_t index /* data block index for sample */, 
                                   timestamp_triple range[2], int index_range[2])
{
  uint64_t int_refval = int_to_u64(st);
  uint64_t lower_int_refval = int_to_u64(range[0].i);
  uint64_t upper_int_refval = int_to_u64(range[1].i);
  if (is_lower_equal_in_quarter_range(int_refval, lower_int_refval)) // still on the left
    return;

  if (is_lower_equal_in_quarter_range(lower_int_refval, int_refval) && is_lower_equal_in_quarter_range(int_refval, upper_int_refval)) // still in range
    return;

  uint64_t evt_refval = evt_to_u64(range[1].e);

  for(int i = index_range[1] + 1; i<input.size(); ++i)
  {
      if (input[i].holds_alternative(TIMESTAMP))
      {
        const timestamp_triple& tt=input[i].get_TIMESTAMP();
        uint64_t et = evt_to_u64(tt.e);
        if (et - evt_refval > uint64_t(1000000000) * 1000) // 1000 seconds before - too far away
          throw("Gap in timestamps in a block too large");
        range[0] = range[1];
        index_range[0] = index_range[1];
        range[1] = tt;
        index_range[1] = i;
        lower_int_refval = int_to_u64(range[0].i);
        upper_int_refval = int_to_u64(range[1].i);
        if (is_lower_equal_in_quarter_range(lower_int_refval, int_refval) && is_lower_equal_in_quarter_range(int_refval, upper_int_refval)) // still in range
          return;
      }
  }
  // if we get here, we have to extrapolate right
  return;
}

void find_matching_timestamp_range(std::vector<varianttype>& input, int_timestamp st, size_t index /* data block index for sample */, 
                                   timestamp_triple range[2], int index_range[2])
{
  uint64_t int_refval = int_to_u64(st);

  int referenceindex=-1;
  uint64_t zeropoint_et;
  
  // function to be called for first frame in a new block (starting at the beginning of a block?)
  // for continuation of frames in the same block simplified version possible, moving to the next timestamp if necessary
  
 //check monotonicity of all timestamps? in some other function at the beginning?
  
  
  //find closest timestamp in a file, so we can use it as an evt reference time
  //for limiting the search
  int delta = -1;
  while(true)
  {
    int nextdelta = (delta<0)? -delta:-(delta+1); //alternately backward-forward
    
    if (!(index+delta >= 0 && index+delta < input.size()))
      if (index+nextdelta >=0 && index+nextdelta < input.size()) //maybe reached end only in one direction
      {
        delta = nextdelta;
        continue;
      }
      else
        break;
      if (input[index+delta].holds_alternative(TIMESTAMP))
      {
	      referenceindex=index+delta;
        const timestamp_triple& tt=input[referenceindex].get_TIMESTAMP();
        zeropoint_et = evt_to_u64(tt.e);
        break;
      }
      delta = nextdelta;
  }
  
  if (referenceindex == -1)
    throw("No timestamps in stream");
  
  //decide, if we should go up or down to find the range
  const timestamp_triple& tt=input[referenceindex].get_TIMESTAMP();
  uint64_t int_zeroval = int_to_u64(tt.i);
  
  bool search_up=false;
  bool search_down=false;
  
  if(is_lower_equal_in_quarter_range(int_zeroval, int_refval))
  {
     search_up = true;
  }

  if(is_lower_equal_in_quarter_range(int_refval, int_zeroval))
  {
     search_down = true;
  }
  
  if (!search_up && !search_down)
    throw("Timestamp in the nearest block too far away");


  if (search_up && search_down)
  {
    cout << "Exact timestamp found 1" << endl;  
          range[0] = range[1] = tt;
          index_range[0] = index_range[1] = referenceindex;
          return;
  }
    
  int lastindex = referenceindex;
  int prevlastindex = -1;
  
  if(search_down)
  {
    for(int i = referenceindex-1; i>=0; --i)
    {
      if (input[i].holds_alternative(TIMESTAMP))
      {
        const timestamp_triple& tt=input[i].get_TIMESTAMP();
        uint64_t et = evt_to_u64(tt.e);
        if (zeropoint_et - et > uint64_t(1000000000) * 1000) // 1000 seconds before - too far away
          throw("Gap in timestamps too large");

        uint64_t int_ts = int_to_u64(tt.i);
  
        if (int_ts == int_refval) // we have found exact timestamp
        {
          cout << "Exact timestamp found" << endl;
          range[0] = range[1] = tt;
          index_range[0] = index_range[1] = i;
          return;
        }
     
        if(is_lower_equal_in_quarter_range(int_ts, int_refval))
        {
          range[0] = tt;
          index_range[0] = i;
          range[1] = input[lastindex].get_TIMESTAMP();
          index_range[1] = lastindex;
          return;
        }
        prevlastindex = lastindex;
        lastindex = i;
      }
    }
    // not found - extrapolate
    if (prevlastindex == -1) // referenceindex is the lowest
    {
      for(int i = referenceindex+1; i<input.size(); ++i)
      {
        if (input[i].holds_alternative(TIMESTAMP))
        {
          const timestamp_triple& tt=input[i].get_TIMESTAMP();
          uint64_t et = evt_to_u64(tt.e);
          if (et - zeropoint_et > uint64_t(1000000000) * 1000) // 1000 seconds before - too far away
            throw("Gap in timestamps too large");
	  range[0] = input[referenceindex].get_TIMESTAMP();
	  index_range[0] = referenceindex;
	  range[1] = input[i].get_TIMESTAMP();
	  index_range[1] = i;
	  return;
        }
      }
      throw("Only one timestamp found");
    }
    range[0] = input[lastindex].get_TIMESTAMP();
    index_range[0] = lastindex;
    range[1] = input[prevlastindex].get_TIMESTAMP();
    index_range[1] = prevlastindex;
    return;
  }

  if(search_up)
  {
    for(int i = referenceindex+1; i<input.size(); ++i)
    {
      if (input[i].holds_alternative(TIMESTAMP))
      {
        const timestamp_triple& tt=input[i].get_TIMESTAMP();
        uint64_t et = evt_to_u64(tt.e);
        if (et - zeropoint_et > uint64_t(1000000000) * 1000) // 1000 seconds before - too far away
          throw("Gap in timestamps too large");

        uint64_t int_ts = int_to_u64(tt.i);
  
        if (int_ts == int_refval) // we have found exact timestamp
        {
          range[0] = range[1] = tt;
          index_range[0] = index_range[1] = i;
          return;
        }
     
        if(is_lower_equal_in_quarter_range(int_refval, int_ts))
        {
          range[1] = tt;
          index_range[1] = i;
          range[0] = input[lastindex].get_TIMESTAMP();
          index_range[0] = lastindex;
          return;
        }
        prevlastindex = lastindex;
        lastindex = i;
      }
    }
    // not found - extrapolate
    if (prevlastindex == -1) //reference index is the highest
    {
      for(int i = referenceindex-1; i>=0; --i)
      {
        if (input[i].holds_alternative(TIMESTAMP))
        {
          const timestamp_triple& tt=input[i].get_TIMESTAMP();
          uint64_t et = evt_to_u64(tt.e);
          if (zeropoint_et - et > uint64_t(1000000000) * 1000) // 1000 seconds before - too far away
            throw("Gap in timestamps too large");
	  range[1] = input[referenceindex].get_TIMESTAMP();
	  index_range[1] = referenceindex;
	  range[0] = input[i].get_TIMESTAMP();
	  index_range[0] = i;
	  return;
        }
      }
      throw("Only one timestamp found");
    }
    range[1] = input[lastindex].get_TIMESTAMP();
    index_range[1] = lastindex;
    range[0] = input[prevlastindex].get_TIMESTAMP();
    index_range[0] = prevlastindex;
    return;
  }

  
}
