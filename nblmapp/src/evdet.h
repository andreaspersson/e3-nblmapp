/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by DMCS for the European Spallation Source ERIC
 * Author: Grzegorz Jablonski
 * 		   DMCS, Lodz, Poland
 */
#ifndef __EVDET_H__
#define __EVDET_H__
#include <ap_fixed.h>
#ifdef  __SYNTHESIS__
#include <hls_stream.h>
#endif

const int nBinsInMTW = 250;


struct triggerInfo
{
  bool pulseTrigger; // true for one cycle at the beginning of the period
  bool beamOnStrobe; // true during beam-on
  bool beamOnPeriodStrobe; // true during entire period with beam
  bool rawDataStrobe; // true during the entire raw data acquisition period - from 3 ms before pulse to 3 ms after pulse
};

template <size_t WIDTH> struct uint_and_triggers
{
  uint_and_triggers() {};
  uint_and_triggers(triggerInfo triggers_, ap_ufixed<WIDTH,WIDTH> data_) : triggers(triggers_), data(data_) {}; 
  triggerInfo triggers;
  ap_ufixed<WIDTH,WIDTH> data; 
};

struct eventInfo
{
  bool TOTvalid;
  bool peakValid;
  bool pileUp;
  bool TOTlimitReached;
  bool isPart2;
  ap_ufixed<9,9> TOT;
  ap_fixed<26,26> Q_TOT;
  ap_fixed<17,17> peakValue;
  ap_ufixed<9,9> TOTstartTime;
  ap_ufixed<9,9> peakTime;
  uint32_t MTWindx;
// the following are not part of the event
  uint8_t negative_saturations;
  uint8_t positive_saturations;
  ap_fixed<25,25> Qtotal;
  bool eventValid;
  bool newFrame;
  uint32_t currentwindow;
  triggerInfo triggers;
};

struct eventInfoForArchiving
{
  bool TOTvalid;
  bool peakValid;
  bool pileUp;
  bool TOTlimitReached;
  bool isPart2;
  ap_ufixed<9,9> TOT;
  ap_fixed<26,26> Q_TOT;
  ap_fixed<17,17> peakValue;
  ap_ufixed<9,9> TOTstartTime;
  ap_ufixed<9,9> peakTime;
  uint32_t MTWindx;
  ap_ufixed<13,13> serialNumber;
  bool eventValid;
};

#ifndef  __SYNTHESIS__ 
inline std::ostream& operator<<(std::ostream& o, const eventInfoForArchiving& e)
{
  o << e.TOTvalid << " " << e.peakValid << " " << e.pileUp << " " << e.TOTlimitReached 
  << " " << e.isPart2 << " " << e.TOT << " " << (int)e.Q_TOT << " " << e.peakValue << " " << e.TOTstartTime << " " <<
  e.peakTime << " " << e.MTWindx << " " << e.serialNumber;
  return o;
}
#endif

struct preprocessedDataItem
{
	uint16_t sample;
	bool newFrame;
	uint8_t sample_index;
	uint32_t frame_index;
	ap_fixed<17,17> adjusted_sample;
	bool belowThr1;
	bool belowThr2;
	bool peakValid;
};

struct sampleAndTimestamp
{
	uint32_t sample;
	uint8_t sample_index;
	uint32_t frame_index;
	triggerInfo triggers;
};

struct preprocessedInfo
{
	uint16_t sample0;
	uint16_t sample1;
	uint8_t sample_index;
	uint32_t frame_index;
};

struct preprocessedData
{
        preprocessedDataItem data0;
        preprocessedDataItem data1;
	triggerInfo triggers;
};

struct pedestalComputationItem
{
    uint16_t sample;
    bool inEvent;
};

struct pedestalComputationData
{
  pedestalComputationItem data0;
  pedestalComputationItem data1;
  triggerInfo triggers;
};

struct pedestalInfo
{
    ap_ufixed<57,57> sum_of_squares;
    ap_ufixed<41,41> sum_of_samples;
    ap_ufixed<25,25> num_of_samples;    
    ap_ufixed<25,25> positive_saturations;
    ap_ufixed<25,25> negative_saturations;
};

struct neutronCounts{
  uint8_t N_n; //neutron count as calculated by the counting method
  ap_fixed<32,32> N_qtot; //neutron count as calculated by the charge method
  uint8_t positive_saturations;
  uint8_t negative_saturations;
  ap_fixed<26,26> Q_background;
  ap_fixed<25,25> Qtotal;
  bool dataValid;
  bool newFrame;
  triggerInfo triggers;
};

struct neutronSummary{
  bool beam;
  ap_ufixed<17,17> count; // 71429 us/14 Hz cycle

  ap_ufixed<49,49> neutron_sum;
  ap_ufixed<32,32> neutron_min;
  ap_ufixed<32,32> neutron_max;
  
  ap_fixed<43,43> Qbackground_sum;
  ap_fixed<26,26> Qbackground_min;
  ap_fixed<26,26> Qbackground_max;
  
  ap_fixed<42,42> Qtotal_sum;
  ap_fixed<25,25> Qtotal_min;
  ap_fixed<25,25> Qtotal_max;
  uint32_t loss_in_window;
};

struct accumulatedSummary_t{
  bool beam;
  uint64_t count; // 71429 us/14 Hz cycle
  uint64_t neutron_sum;
  uint64_t neutron_min;
  uint64_t neutron_max;
  int64_t Qbackground_sum;
  int64_t Qbackground_min;
  int64_t Qbackground_max;
  int64_t Qtotal_sum;
  int64_t Qtotal_min;
  int64_t Qtotal_max;
  int64_t loss_in_window;
};

struct eventStatistics_t{
  uint32_t TOTmin;
  uint32_t TOTmax;
  double   TOTaverage;
  double   TOTrms;
  int32_t  peakValueMin;
  int32_t  peakValueMax;
  double   peakValueAverage;
  double   peakValueRms;
  int32_t  Q_TOTmin;
  int32_t  Q_TOTmax;
  double   Q_TOTaverage;
  double   Q_TOTrms;
  uint16_t eventCount;
};

struct lossInUserDefinedWindow_t{
  uint64_t lossDecimation_factor;
  uint32_t lossWindow_position;
  uint32_t lossWindow_samples;
  uint32_t lossWords;
	std::vector<double> lossData_avg;
	std::vector<double> lossData_max;
};

struct protectionFunctionOutputs_t{
  uint64_t protectionDecimation_factor;
  uint32_t protectionWindow_position;
  uint32_t protectionWindow_samples;
  uint32_t protectionWords;
	std::vector<double> protectionAvg0_max;
	std::vector<double> protectionAvg1_max;
	std::vector<double> protectionRelaxation_max;
	std::vector<double> protectionXY_count;
};

#ifdef  __SYNTHESIS__
void
preprocess (hls::stream<sampleAndTimestamp>& A, hls::stream<preprocessedData>& E, hls::stream<preprocessedInfo>& sample_stream, ap_fixed<17,17> neutronAmpl_min,
		ap_fixed<17,17> eventDetection_thr, ap_fixed<17,17> eventDetection_thr2, uint16_t pedestal);
void
detect (hls::stream<preprocessedData>& A, hls::stream<eventInfo>& E, hls::stream<eventInfo>& E2, hls::stream<pedestalComputationData>& PC, hls::stream<eventInfoForArchiving>& event_stream, uint16_t neutronTOT_min_indx, uint16_t pileUpTOT_start_indx);

void
neutron_aligner (hls::stream<eventInfo>& E, hls::stream<eventInfo>& New, hls::stream<eventInfo>& Old);


void
neutron_counter (hls::stream<eventInfo>& New, hls::stream<eventInfo>& Old, hls::stream<neutronCounts>& N, ap_ufixed<32,0> inverse_of_neutron_charge);

void
neutron_summarizer (hls::stream<neutronCounts>& N, hls::stream<neutronCounts>& E, hls::stream<neutronCounts>& E2, hls::stream<neutronCounts>& E3, hls::stream<neutronCounts>& E4, hls::stream<neutronCounts>& E5);

const unsigned counts_per_V = 65535;

void
toplevel (hls::stream<uint32_t>& data_stream, hls::stream<neutronCounts>& nc2_stream, hls::stream<preprocessedInfo>& sample_stream, hls::stream<eventInfo>& event_stream, ap_fixed<17,17> neutronAmpl_min, uint16_t neutronTOT_min_indx, uint16_t pileUpTOT_start_indx,
          ap_fixed<17,17> eventDetection_thr, ap_fixed<17,17> eventDetection_thr2, uint16_t pedestal, ap_ufixed<32,0> inverse_of_Q_TOT_single_neutron);

void
pedestal_computer (hls::stream<pedestalComputationData>& PC, hls::stream<pedestalInfo>& PI, ap_ufixed<25,25> window_start,
    ap_ufixed<25,25> window_length, bool excludeEvents);


void
loss_waveform (hls::stream<neutronCounts>& N, hls::stream<ap_ufixed<128,128> >& sample_stream, hls::stream<ap_ufixed<128,128> >& config_word, 
  ap_ufixed<17,17> window_position, ap_ufixed<10,10> window_samples, ap_ufixed<3,3> decimation_factor);

void
loss_summarizer (hls::stream<neutronCounts>& N, hls::stream<neutronSummary >& E, ap_ufixed<17,17> window_position, ap_ufixed<10,10> window_samples);

void
neutron_totaler (hls::stream<neutronCounts>& N, hls::stream<uint_and_triggers<32> >& n_total);

#endif /* __SYNTHESIS__ */

#endif /* __EVDET_H__ */
