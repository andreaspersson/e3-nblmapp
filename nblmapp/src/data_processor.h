/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by DMCS for the European Spallation Source ERIC
 * Author: Grzegorz Jablonski
 * 		   DMCS, Lodz, Poland
 * 
 * Modified by CEA-ESS for the European Spallation Source ERIC
 * Author: Yannick Mariette
 *         CEA, Saclay, France
 */
#ifndef __DATA_PROCESSOR_H__
#define __DATA_PROCESSOR_H__
#include "buffers.h"
#include "evdet.h"
#include "u256.h"
#include "crc32.h"
#include <inttypes.h>
#include "unistd.h"
#include "IFC14AIChannel.h"
#ifdef USE_HDF5
#include "hdf5_interface.h"
#endif
#include "circular_buffer_blocking.h"
#include "interleaver_thread.h"

uint64_t
u256_get_data_and_shift_int (u256 * d, int bits, uint8_t * buffer,
			     uint32_t * offset, uint32_t buffer_size);
ap_ufixed<64,64> make_ufixed_64_from_uint64(uint64_t arg);
void timestamp_sanity_check(std::vector<varianttype>& input);
void find_matching_timestamp_range(std::vector<varianttype>& input, int_timestamp st, size_t index /* data block index for sample */, 
                                   timestamp_triple range[2], int index_range[2]);
void find_next_matching_timestamp_range_same_block(std::vector<varianttype>& input, int_timestamp st, size_t index /* data block index for sample */, 
                                   timestamp_triple range[2], int index_range[2]);


class DataProcessor
{
  enum stream_interpreter_state_t
    { SIS_SEARCHING, SIS_DATA, SIS_CRC };
  uint32_t mtw_index;
  uint8_t sample_index;
  int ch;
  ap_ufixed<13,13> lastSerialNumber;

  uint64_t last_event_mtw;
  uint8_t current_event_channel;
  uint32_t current_window;
  uint32_t prev_MTWindx;
  uint32_t wraparounds;

  uint32_t last_neutron_window = 1;
  uint32_t last_neutron_channel = 5;

  uint8_t frame_buffer[MAXFRAMESIZE];
  uint32_t data_size;
  enum stream_interpreter_state_t sis = { SIS_SEARCHING };
  struct u256 data;
  int last_index = -1;  // last event index

  // timestamping state
  bool real_timestamps = true;
  std::vector < varianttype > *input_ptr = NULL;
  bool newblock;
  timestamp_triple range[2];
  int index_range[2];
  size_t block_index;

public:
  DataProcessor(int channel, std::vector<std::shared_ptr<IFC14AIChannel> > m_AIChannels) : ch(channel), AIChannels(m_AIChannels) {};
  
CircularBufferBlocking<uint8_t> buffers;
CircularBufferBlocking<varianttype> buffers_timestamped;

bool fillFile= false;
volatile bool exit_processData = false;
bool enableHDF5;
FILE* outfile;

  //void setChannel(int channel) {ch = channel;};
  void process_event (struct u256 *d, timestamp_triple range[2]);
  void process_neutron (struct u256 *d, timestamp_triple range[2]);
  void process_sample (struct u256 *d, uint32_t header_mtw,	uint8_t header_sample_idx, timestamp_triple range[2]);
  void process_stream_data_item (struct u256 *d, uint32_t header_mtw,
			  uint8_t header_sample_idx, timestamp_triple* range = NULL);
  void process_statistics (uint8_t * buf, uint32_t data_size, int buffer_size);
  void processOfflineDataInterleaved (uint16_t timeout_ms);
  int  processOnlineData (uint32_t timeout_ms);
  uint64_t get_evt_ts (int_timestamp st);
  bool get_data_from_buffer(std::vector<varianttype>& input, uint8_t *byteArray, size_t& index, size_t& offset, size_t&  bytes_to_read);

private:
  std::vector<std::shared_ptr<IFC14AIChannel> > AIChannels;

  bool get_fillFile(void) {return fillFile;};
};



void interleaver_thread(CircularBufferBlocking<uint8_t>& cb_raw, 
                        CircularBufferBlocking<timestamp_triple>& cb_evt, 
                        CircularBufferBlocking<varianttype> &output,
                        int channel,
                        volatile bool& exitThread, 
                        condRead_t& readCB,
                        ifcdaqdrv_usr_t& deviceUser);

#endif /* __DATA_PROCESSOR_H__ */
