/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by DMCS for the European Spallation Source ERIC
 * Author: Grzegorz Jablonski
 * 		   DMCS, Lodz, Poland
 * 
 */
#include "crc32.h"
#include <stdio.h>

static uint32_t table[256];

void
crc32_init ()
{
  unsigned int i, j;
  unsigned int c;

  uint32_t poly =
    1 | 1 << 1 | 1 << 2 | 1 << 4 | 1 << 5 | 1 << 7 | 1 << 8 | 1 << 10 | 1 <<
    11 | 1 << 12 | 1 << 16 | 1 << 22 | 1 << 23 | 1 << 26;


  for (i = 0; i < 256; i++)
    {
      for (c = i << 24, j = 8; j > 0; --j)
	c = c & 0x80000000 ? (c << 1) ^ poly : (c << 1);

      table[i] = c;
    }
}

void
crc32_print ()
{
  uint16_t i;
  printf ("table[] =\n{\n");
  for (i = 0; i < 256; i += 4)
    {
      printf ("  0x%08x, 0x%08x, 0x%08x, 0x%08x", table[i + 0], table[i + 1],
	      table[i + 2], table[i + 3]);
      if (i + 4 < 256)
	putchar (',');
      putchar ('\n');
    }
  printf ("};\n");
}

uint32_t
crc32 (const uint32_t init, const uint8_t buf[16])
{
  int8_t i;
  uint32_t crc = init;

  for (i = 0; i < 16; ++i)
    crc = (crc << 8) ^ table[((crc >> 24) ^ buf[i]) & 0xFF];

  return crc;
}

inline uint32_t
crc_8b (const uint32_t init, const uint8_t b)
{
  uint32_t crc = init;

  crc = (crc << 8) ^ table[((crc >> 24) ^ b) & 0xFF];

  return crc;
}
