/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by DMCS for the European Spallation Source ERIC
 * Author: Grzegorz Jablonski
 * 		   DMCS, Lodz, Poland
 * 
 * Modified by CEA-ESS for the European Spallation Source ERIC
 * Author: Yannick Mariette
 *         CEA, Saclay, France
 */
#ifndef __CONFIG_H__
#define __CONFIG_H__

#include <stdint.h>
#include <signal.h>

#define SCOPE_RAW_DATA_SAMPLES_MAX  10000 // The same value is also set the nblmapp.cmd -> NELM
#define CB_CHANNEL_NB_MAX 14
#define NUM_RAW_CHANNELS 6
#define RAWBUFSIZE 64*1024*1024//1024*1024*1024
#define KBUF_SIZE (4 * 1024 * 1024)
#define INTERLEAVED_TIMESTAMP_STEP  1000 //in µs
#define INTERLEAVED_PERIODIC_BLOCK_SIZE (1024*1024)
#define INTERLEAVED_BLOCK_SIZE (64*1024)// Size of data in a "data block"
#define INTERLEAVED_BLOCK_SIZE_CONTINOUS_RAWDATA (64*INTERLEAVED_BLOCK_SIZE)// Size of data in a "data block" when REG_DECIMATOR_START an REG_DECIMATOR_STOP are 0

const unsigned int data_item_sizes[CB_CHANNEL_NB_MAX] =
  { 120, 120, 120, 120, 120, 120, 107, 128, 32, 32, 32, 32, 32, 32 };

const double relative_buffer_sizes[CB_CHANNEL_NB_MAX] =
  { 20,  20,  20,  20,  20,  20,  30, 1, 50, 50, 50, 50, 50, 50 };
#define BUFSIZE (52*1024*1024)
#define MAXFRAMESIZE (128*1024)

//const uint32_t channels_in_cb_default[2] = { 0b00011101000111, 0b11100010111000 }; // Default location: channels are located in which block ram
extern const uint32_t channels_in_cb[2]; // which channels are located in which block ram
#define channels_in_thread channels_in_cb

const int cb_channel_events = 0;
const int cb_channel_rawdata = 8;
const int cb_channel_neutrons = 6;
const int cb_channel_periodic = 7;

//#define DEBUG_RAW_VALUES
#ifdef __x86_64__
#define USE_HDF5
#endif

//#define USE_TXTFILES
#ifdef __x86_64__
//#define ENABLE_DATA_CHECK
#endif

//#define ASSUME_CONSTANT_TRIGGER_PERIOD
//#define TRIGGER_PERIOD 100000000 /* 10 Hz nanoseconds  */
#define TRIGGER_PERIOD 71429016 /* nanoseconds, can be more than 1'000'000'000  */

template <class T, class U> T CONVERT_mV    (U data) {return (T)(1000*(double)data/65535 - 500);};
template <class T, class U> T CONVERT_ns    (U data) {return (T)((double)data*4);};
template <class T, class U> T CONVERT_Q     (U data) {return (T)(4*1000*(double)data/65535 + 500/65535);};
//template <class T, class U> T CONVERT_Q_unsigned     (U data) {return (T)(4*1000*(double)data/65535 - 500);};

#endif /* __CONFIG_H__ */
