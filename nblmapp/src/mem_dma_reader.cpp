/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by DMCS for the European Spallation Source ERIC
 * Author: Grzegorz Jablonski
 * 		   DMCS, Lodz, Poland
 * 
 * Modified by CEA-ESS for the European Spallation Source ERIC
 * Author: Yannick Mariette
 *         CEA, Saclay, France
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/types.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <errno.h>
#include <getopt.h>
#include <stdint.h>
#include <pthread.h>
#include <inttypes.h>
#include <signal.h>
#include <thread>
#include <mutex>

//#include "args.h"
//#include "helpers.h"
//#include  "ucsr.h"
//#include "dma_cmd.h"
#include "u256.h"
#include "crc32.h"

#include "data_source.h"
#include "evdet.h"



#include "buffers.h"

#include "IFC14.h"
#include "IFC14AIChannelGroup.h"
#include "IFC14AIChannel.h"

//Initialization : channels_in_cb[] = channels_in_cb_default[]
const uint32_t channels_in_cb[2] = { 0b00011101000111, 0b11100010111000 }; // which channels are located in which block ram

//uint64_t data_read_counter_g[2][CB_CHANNEL_NB_MAX];
//uint64_t data_read_counter[CB_CHANNEL_NB_MAX];

std::mutex cb_reg_bank_mutex;

void
write_banked_reg (unsigned reg_selector, unsigned channel,
		  uint32_t value, ifcdaqdrv_usr_t& deviceUser)
{
  std::lock_guard<std::mutex> lock(cb_reg_bank_mutex);
  write_reg (REG_CBRS, (reg_selector << 0) | (channel << 16), deviceUser);
  write_reg (REG_CBRV, value, deviceUser);
}


void
write_am_banked_reg (unsigned reg_selector, unsigned channel,
		     uint32_t value, ifcdaqdrv_usr_t& deviceUser)
{
  write_reg (REG_AMRS, (reg_selector << 0) | (channel << 16), deviceUser);
  write_reg (REG_AMRV, value, deviceUser);
}

uint32_t
read_am_banked_reg (unsigned reg_selector, unsigned channel, ifcdaqdrv_usr_t& deviceUser)
{
  write_reg (REG_AMRS, (reg_selector << 0) | (channel << 16), deviceUser);
//  printf("Mux: %d\n",read_reg (REG_AMRS, deviceUser));
  return read_reg (REG_AMRV, deviceUser);
}

uint32_t
read_banked_reg (unsigned reg_selector, unsigned channel, ifcdaqdrv_usr_t& deviceUser)
{
  std::lock_guard<std::mutex> lock(cb_reg_bank_mutex);
  write_reg (REG_CBRS, (reg_selector << 0) | (channel << 16), deviceUser);
  return read_reg (REG_CBRV, deviceUser);
}

void IFC14AIChannelGroup::initialize_channel_arrays (void)
{
  for(int b=0; b<2; ++b)
  {
    uint32_t addr = 128 * 1024; // 0x20000

    double sum = 0;
    for (int i = 0; i < CB_CHANNEL_NB_MAX; ++i)
      if (channels_in_cb[b] & (1 << i))
      {
        sum += relative_buffer_sizes[i];
      }

    printf("\nDDR FPGA bank %d:\n", b); 
    for (int i = 0; i < CB_CHANNEL_NB_MAX; ++i)
      if (channels_in_cb[b] & (1 << i))
      {
        buf_beg[i] = addr;
        read_addr[i] = addr;
        uint32_t size = relative_buffer_sizes[i] / sum * (512 * 1024 * 1024 - 0x20000);
        // Set FPGA DDR CB smaller than the CPU big DDR buffer for the same CB channel
        if (size > bufsizes[i])
          size = bufsizes[i];
        size &= ~(0xfff); // at the 4k boundary
        printf("buf_size[%d] = %" PRIu32" kB\n", i, size/(1024));
        addr += size;
        buf_end[i] = addr;
        // printf("buf_beg[%d] = 0x%08" PRIx32"\n", i, buf_beg[i]); 
        // printf("buf_end[%d] = 0x%08" PRIx32"\n", i, buf_end[i]); 
//        data_read_counter[i] = 0;
      }
  }
}



//-------------------------------------------------------------------------------------------------
// result in micro-seconds
double rTime ()
{
  struct timeval tt;

  gettimeofday (&tt, (struct timezone *) 0);
  return (double) 1000000.0 *(double) tt.tv_sec + (double) tt.tv_usec;
}


uint64_t u256_get_data_and_shift_int (u256 * d, int bits, uint8_t * buffer,
			     uint32_t * offset, uint32_t buffer_size)
{
  if ((d->bits <= 128) and (*offset < buffer_size))
    {
      u256_shift_in (d, buffer + *offset);
      *offset += 16;
    }
  return u256_get_top_bits_and_cut (d, bits);
}


ap_ufixed<64,64> make_ufixed_64_from_uint64(uint64_t arg)
{
  return (ap_ufixed<64,64>((arg >> 32) & 0xffffffff) << 32) | ap_ufixed<64,64>(arg & 0xffffffff);
}



void IFC14AIChannelGroup::allocateBuffers(void)
{
  int chancount = 0;
  int i;
  for(i=0; i<CB_CHANNEL_NB_MAX; ++i)
    if ( (1<<i) & channelMask)
      ++chancount;
      
  double sum = 0;
  for (int i = 0; i < CB_CHANNEL_NB_MAX; ++i)
  {
    if ( (1<<i) & channelMask)
      sum += relative_buffer_sizes[i];
  }

  printf("\nBig DDR area:\n");
  for(i=0; i<CB_CHANNEL_NB_MAX; ++i)
  {
    if ( (1<<i) & channelMask)
    {
//      bufsizes[i] = uint32_t((relative_buffer_sizes[i] / sum) * RAWBUFSIZE) & 0xfffffff0ul;
      bufsizes[i] = (uint32_t((relative_buffer_sizes[i] / sum) * m_deviceUser.DOD_MemSize) / INTERLEAVED_BLOCK_SIZE)* INTERLEAVED_BLOCK_SIZE;
      //printf("Resize buffers[%d]\n", i);
      //printf("Resize buffers_timestamped[%d]\n", i);
      processors.at(i)->buffers.resize(bufsizes[i]);
      // buffers_timestamped is a variant type circular buffer
      // A block contains "buffers data ptr" and "timestamp data" (set enough blocks for "buffers data ptr" and "timestamp")
      processors.at(i)->buffers_timestamped.resize((bufsizes[i]/INTERLEAVED_BLOCK_SIZE)+(INTERLEAVED_BLOCK_SIZE*bufsizes[i]*4*14/1000000)/INTERLEAVED_BLOCK_SIZE + INTERLEAVED_BLOCK_SIZE);
#if 0      
      if (mlock (processors.at(i)->buffers, bufsizes[i]))
      {
        perror ("mlock");
        exit(1);
      }
#endif
    }
    else
    {
      bufsizes[i]=0;
      processors.at(i)->buffers.clear();
      processors.at(i)->buffers_timestamped.clear();
      //printf("Clear buffers[%d]\n", i);
      //printf("Clear buffers_timestamped[%d]\n", i);
    }
    printf("bufsizes[%d] = %" PRIu32" kB\n", i, (uint32_t)(bufsizes[i]/(1024))); 
  }

#ifdef USE_TXTFILES
std::string filenames[] = {
    "event0.txt",
    "event1.txt",
    "event2.txt",
    "event3.txt",
    "event4.txt",
    "event5.txt",
    "neutrons.txt",
    "perdata.txt",
    "rawdata0.txt",
    "rawdata1.txt",
    "rawdata2.txt",
    "rawdata3.txt",
    "rawdata4.txt",
    "rawdata5.txt"
    };

  for(auto i=0u;i<CB_CHANNEL_NB_MAX;++i)
  {
    if (i< sizeof(filenames)/sizeof(string))
    {
      std::string fullFileName = std::string("ifc") + std::to_string(m_deviceUser.card) + std::string("_") + filenames[i];
      processors.at(i)->outfile=fopen(fullFileName.c_str(),"w");
      if (processors.at(i)->outfile == nullptr)
        perror("Cannot open file");
    }

    //processors.at(i)->setChannel(i);
  }
#endif
}

void IFC14AIChannelGroup::freeBuffers()
{
  int i;
  for(i=0; i<CB_CHANNEL_NB_MAX; ++i)
  {
    processors.at(i)->buffers.clear();
    processors.at(i)->buffers_timestamped.clear();
    //printf("Clear buffers[%d]\n", i);
    //printf("Clear buffers_timestamped[%d]\n", i);

#ifdef USE_TXTFILES
    if(processors.at(i)->outfile)
      fclose(processors.at(i)->outfile);
#endif
  }
  fprintf(stderr, "Buffers freed\n");
}

void IFC14AIChannelGroup::copyDataStream(int ch, int t)
{
  size_t bytes_written=0;
  volatile bool enable_copy = true;
    
    // Stop writing data into the big buffer when DOD request
    if(ch != cb_channel_periodic)
    {
      pthread_mutex_lock (&readNonPeriodicCB.mutexRetrieveData);
      if(readNonPeriodicCB.retieveData)
        enable_copy = false;
      pthread_mutex_unlock (&readNonPeriodicCB.mutexRetrieveData);
    }
    
    // Copy from "small" CPU DDR buffer to a bigger circular buffer
    if (stalled_buffer[ch] == 0)
    {
      size_t size = size_g[checked_buffer[t]][ch];
      
      if(enable_copy)
      {
        processors.at(ch)->buffers.put_data(m_deviceUser.device.Kbuffer[checked_buffer[t]][ch], size, bytes_written);
      }
      else
      {
        bytes_written = size;
      }
      
      //printf("put %d data in buffers CB%d\n",bytes_written, ch);
      size_r[checked_buffer[t]][ch] = bytes_written;

      if (size != bytes_written)
      {
        std::cout << "stalled ch=" << (int)ch <<std::endl;
        stalled_buffer[ch] = checked_buffer[t] + 1;
  //      std::cout << "size=" << size << " bytes_written=" <<  bytes_written << std::endl;
  //      std::cout << "size=" << size << " bytes_written=" <<  size_r[checked_buffer[t]][ch] << std::endl;
      }
      else
        stalled_buffer[ch] = 0;
    }
    else if (stalled_buffer[ch] == checked_buffer[t] + 1)
    {
    //  std::cout << "channel=" << (int)ch << " size_g=" << size_g[checked_buffer[t]][ch] << " size_r=" <<  size_r[checked_buffer[t]][ch]<< std::endl;
      if (size_g[checked_buffer[t]][ch] < size_r[checked_buffer[t]][ch])
      {
        std::cout << "Wrong size! ch=" << ch << " stalled_buffer[ch] =" << stalled_buffer[ch] << "size_g[checked_buffer[t]][ch]=" 
        << size_g[checked_buffer[t]][ch] << " size_r[checked_buffer[t]][ch]=" << size_r[checked_buffer[t]][ch] << std::endl;
      }
      else
      {
        size_t size = size_g[checked_buffer[t]][ch] - size_r[checked_buffer[t]][ch];

        if(enable_copy)
        {
          processors.at(ch)->buffers.put_data(((uint8_t*)m_deviceUser.device.Kbuffer[checked_buffer[t]][ch] + size_r[checked_buffer[t]][ch]), size, bytes_written);
        }
        else
        {
          bytes_written = size;
        }
        
        
        size_r[checked_buffer[t]][ch] += bytes_written;
    //    std::cout << "size=" << size << " bytes_written=" <<  bytes_written << " size_r=" << size_r[checked_buffer[t]][ch] << std::endl;

        if (size != bytes_written)
          stalled_buffer[ch] = checked_buffer[t] + 1;
        else
          stalled_buffer[ch] = 0;
      }
    }
    else
      size_r[checked_buffer[t]][ch] = 0;
}



void IFC14AIChannelGroup::buffer_checker (int t)
{
  int ch;
  uint32_t chan_mask = channelMask;
  //struct timespec max_wait = {1, 0};
  struct timespec timeout;
  int result;

  printf("Starting buffer_checker(%d) task\n", t);
  while (!exit_loop && !exit_processData_loop)
    {
      // Waiting for the signal from the main_loop thread indicating that data
      // has been transfered into the small DDR area
      pthread_mutex_lock (&reader_mutex[t]);
      clock_gettime(CLOCK_REALTIME, &timeout);
      timeout.tv_sec += 1;
      result = 0;
      while ((checked_buffer[t] == current_buffer[t]) && (!exit_loop) && (!exit_processData_loop))
      {
        result = pthread_cond_timedwait (&condvar[t], &reader_mutex[t], &timeout);
        if (result == ETIMEDOUT)
        {
          clock_gettime(CLOCK_REALTIME, &timeout);
          timeout.tv_sec += 1;
        }
      }
      pthread_mutex_unlock (&reader_mutex[t]);

      if( (!exit_loop) && (!exit_processData_loop) )
      {
        // Now we will start to transfer data fom the small CPU DDR area to the bigger one
        for (ch = 0; ch < CB_CHANNEL_NB_MAX; ++ch)
          // Treat only CB channels which received data into the FPGA DDR bank "t"
          if ((channels_in_thread[t] & (1 << ch)))
          {
  //          printf("Channel %d in buffer_checker\n", t);
            if ( !(channels_active[t][checked_buffer[t]] & (1 << ch)) || !((1<<ch) & chan_mask) )
              continue;
            copyDataStream (ch, t);
          }

        // Sending the signal to the main_loop thread to indicate that data has been
        // transfered from the small DDR area to the bigger one
        pthread_mutex_lock (&reader_mutex[t]);
        checked_buffer[t] = !checked_buffer[t];
        pthread_cond_signal (&condvar[t]);
        pthread_mutex_unlock (&reader_mutex[t]);
      }
    }
  printf("End of buffer_checker(%d) task\n", t);
}

void reset_channels (uint32_t channel_mask, ifcdaqdrv_usr_t& deviceUser)
{
  uint32_t all_channel = (1 << CB_CHANNEL_NB_MAX) - 1;
  uint32_t tmp_val;

  // Write '0' for disabling. Disable all channels
  write_reg (REG_CB_DATA_CHANNEL_ENABLE, 0, deviceUser);
  while (0 != read_reg (REG_CB_DATA_CHANNEL_ENABLE, deviceUser))
    printf ("Waiting for disable REG_CB_DATA_CHANNEL_ENABLE\n"); // wait for disable to complete
  
  // Write '1' (reset for all channels) then '0' (run activated channels) in REG_CB_DATA_CHANNEL_RESET:
  write_reg (REG_CB_DATA_CHANNEL_RESET, all_channel, deviceUser);
  while (all_channel != read_reg (REG_CB_DATA_CHANNEL_RESET, deviceUser))
    printf ("Waiting for reset REG_CB_DATA_CHANNEL_RESET\n");	// wait for reset to complete

  tmp_val = all_channel & (~(channel_mask));
  write_reg (REG_CB_DATA_CHANNEL_RESET, tmp_val, deviceUser);
  while (tmp_val != read_reg (REG_CB_DATA_CHANNEL_RESET, deviceUser))
    printf ("Waiting for run REG_CB_DATA_CHANNEL_RESET\n"); // wait for reset off
}


bool DataProcessor:: get_data_from_buffer(std::vector<varianttype>& input, uint8_t *byteArray, size_t& index, size_t& offset, size_t&  bytes_to_read)
{
  size_t bytes_read = 0;

  while(bytes_read < bytes_to_read)
  {
    while(index < input.size())
    {
      if (input[index].holds_alternative(TIMESTAMP))
      {
        //printf("Timestamp found in data\n");
        index++;
      }
      else
        break;
    }
    
    if(index >= input.size())
    {
     return false;
    }
  
    const uint8_t* v = input[index].get_DATA();
    
    size_t bytestoread = input[index].data_size() - offset;
    if (bytestoread > bytes_to_read-bytes_read)
      bytestoread = bytes_to_read-bytes_read;
    
    memcpy(byteArray+bytes_read, v + offset, bytestoread);
    
    bytes_read += bytestoread; 
    offset += bytestoread;
    if (offset >= input[index].data_size())
    {
      ++index;
      offset = 0;
    }
  }
  return true;
}
