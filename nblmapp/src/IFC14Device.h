/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Original file created by the European Spallation Source ERIC
 * Author: Joao Paulo Martins 
 * 		   ESS, Lund, Sweden
 * 
 * Modified by CEA-ESS for the European Spallation Source ERIC
 * Author: Yannick Mariette
 *         CEA, Saclay, France
 */
#ifndef IFC14DEVICE_H
#define IFC14DEVICE_H

#include <nds3/nds.h>

#include <nblmdrv.h>

#include "IFC14AIChannelGroup.h"
#include "IFC14AIChannel.h"

class IFC14Devices
{
  public:
    IFC14Devices(nds::Factory& factory, const std::string &deviceName, const nds::namedParameters_t& parameters);
    ~IFC14Devices();

  private:
    std::vector<std::shared_ptr<IFC14AIChannelGroup> > m_AIChannelGroups;
    //std::vector<ifcdaqdrv_usr_t> m_deviceUser;
    nds::Node m_node;
};

#endif /* IFC14DEVICE_H */
